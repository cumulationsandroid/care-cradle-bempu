package com.bempu.carecradle.models;

public class VideoCountResponse {
    private String count;
    private String status;

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ClassPojo [count = " + count + ", status = " + status + "]";
    }
}