package com.bempu.carecradle.models;

import java.util.List;

/**
 * Created by Amit Tumkur on 23-05-2018.
 */

public class DeviceDataRequest {
    private String uniqueid;
    private String r_time;
    private List<HardwareData> hardware_data;

    public String getUniqueid() {
        return uniqueid;
    }

    public void setUniqueid(String uniqueid) {
        this.uniqueid = uniqueid;
    }

    public String getR_time() {
        return r_time;
    }

    public void setR_time(String r_time) {
        this.r_time = r_time;
    }

    public List<HardwareData> getHardware_data() {
        return hardware_data;
    }

    public void setHardware_data(List<HardwareData> hardware_data) {
        this.hardware_data = hardware_data;
    }
}
