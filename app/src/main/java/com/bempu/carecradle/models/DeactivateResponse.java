package com.bempu.carecradle.models;

/**
 * Created by Amit T on 21-09-2017.
 */

public class DeactivateResponse {
    private String status;

    private String description;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "ClassPojo [status = " + status + ", description = " + description + "]";
    }
}
