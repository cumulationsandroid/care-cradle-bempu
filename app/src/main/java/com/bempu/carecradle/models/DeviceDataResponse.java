package com.bempu.carecradle.models;

/**
 * Created by Amit Tumkur on 23-05-2018.
 */

public class DeviceDataResponse {
    private String status;
    private String records;
    private String custom_message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRecords() {
        return records;
    }

    public void setRecords(String records) {
        this.records = records;
    }

    public String getCustom_message() {
        return custom_message;
    }

    public void setCustom_message(String custom_message) {
        this.custom_message = custom_message;
    }
}
