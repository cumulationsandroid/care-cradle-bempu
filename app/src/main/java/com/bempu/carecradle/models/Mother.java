package com.bempu.carecradle.models;

/**
 * Created by Amit T on 18-09-2017.
 */

public class Mother {
    private String name;

    public Mother(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
