package com.bempu.carecradle.models;

import android.app.Notification;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.NotificationCompat;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;

/**
 * Created by Amit T on 21-09-2017.
 */

public class Upload implements Parcelable{
    public static final String UPLOADING = "uploading";
    public static final String COMPLETED = "completed";
    public static final String FAILED = "failed";
    public static final String DELETED = "deleted";

    public Upload(){

    }

    private int progress;
    private int currentFileSize;
    private int totalFileSize;
    private int notificationId;
    private String filePath;

    /*For S3*/
    private TransferObserver transferObserver;

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public int getCurrentFileSize() {
        return currentFileSize;
    }

    public void setCurrentFileSize(int currentFileSize) {
        this.currentFileSize = currentFileSize;
    }

    public int getTotalFileSize() {
        return totalFileSize;
    }

    public void setTotalFileSize(int totalFileSize) {
        this.totalFileSize = totalFileSize;
    }

    public int getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(int notificationId) {
        this.notificationId = notificationId;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public TransferObserver getTransferObserver() {
        return transferObserver;
    }

    public void setTransferObserver(TransferObserver transferObserver) {
        this.transferObserver = transferObserver;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeInt(progress);
        dest.writeInt(currentFileSize);
        dest.writeInt(totalFileSize);
    }

    private Upload(Parcel in) {

        progress = in.readInt();
        currentFileSize = in.readInt();
        totalFileSize = in.readInt();
    }

    public static final Parcelable.Creator<Upload> CREATOR = new Parcelable.Creator<Upload>() {
        public Upload createFromParcel(Parcel in) {
            return new Upload(in);
        }

        public Upload[] newArray(int size) {
            return new Upload[size];
        }
    };
}
