package com.bempu.carecradle.models;

/**
 * Created by Amit Tumkur on 10-04-2018.
 */

public class FailedUploadEvent {
    private String uId;
    private String model;
    private String carrier;
    private String networkType;
    private String netStrength;
    private String failure;
    private String uploadTime;
    private String failureTime;
    private String netSpeed;
    private String bytesFailed;
    private String name;
    private String size;
    private String uploadPercent;

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public String getNetworkType() {
        return networkType;
    }

    public void setNetworkType(String networkType) {
        this.networkType = networkType;
    }

    public String getNetStrength() {
        return netStrength;
    }

    public void setNetStrength(String netStrength) {
        this.netStrength = netStrength;
    }

    public String getFailure() {
        return failure;
    }

    public void setFailure(String failure) {
        this.failure = failure;
    }

    public String getUploadTime() {
        return uploadTime;
    }

    public void setUploadTime(String uploadTime) {
        this.uploadTime = uploadTime;
    }

    public String getFailureTime() {
        return failureTime;
    }

    public void setFailureTime(String failureTime) {
        this.failureTime = failureTime;
    }

    public String getNetSpeed() {
        return netSpeed;
    }

    public void setNetSpeed(String netSpeed) {
        this.netSpeed = netSpeed;
    }

    public String getBytesFailed() {
        return bytesFailed;
    }

    public void setBytesFailed(String bytesFailed) {
        this.bytesFailed = bytesFailed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getUploadPercent() {
        return uploadPercent;
    }

    public void setUploadPercent(String uploadPercent) {
        this.uploadPercent = uploadPercent;
    }
}
