package com.bempu.carecradle;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Camera;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.bempu.carecradle.models.RegisterResponse;
import com.bempu.carecradle.models.Upload;
import com.bempu.carecradle.s3uploads.S3Constants;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * Created by Amit T on 21-09-2017.
 */

public class Utils {
    private static Utils utils;
    private AlertDialog enableNetworkDialog;
    private ProgressDialog progressDialog;
    private int notificationId;
    private static final String VIDEO_NAME_DATE_FORMAT = "dd-MM-yyyy-HHmmss";
    private static final String S3_BASE_URL = "https://s3.amazonaws.com";
    private static final String URL_DELIMITER = "/";
    private static final String VIDEO_HEADER_DATE_FORMAT = "yyyy/MM/dd HH:mm:ss";
    private String lastVideoHeaderTime;
    private List<String> supportedFocusModes;

//    public static final int UPLOAD_COMPLETED_GKEY = 11;
//    public static final int UPLOADING_GKEY = 12;
//    public static final int UPLOAD_FAIlED_GKEY = 13;

    public static final int IN_PROGRESS = 1;
    public static final int FAILED = 0;

    private Utils() {
    }

    public static Utils getUtils() {
        if (utils == null)
            utils = new Utils();
        return utils;
    }

    public void saveCredentials(Context context, RegisterResponse response) {
        context.getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE)
                .edit()
                .putString(Constants.USER_UNIQUE_ID, response.getUniqueid())
                .putString(Constants.USER_NAME, response.getName())
                .putString(Constants.USER_API_KEY, response.getApi_key())
                .putBoolean(Constants.IS_S3_UPLOAD, false)
                .apply();
    }

    public String getUserUniqueId(Context context) {
        return context.getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE)
                .getString(Constants.USER_UNIQUE_ID, "");
    }

    public String getUserApiKey(Context context) {
        return context.getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE)
                .getString(Constants.USER_API_KEY, "");
    }

    public boolean isS3Upload(Context context) {
        return context.getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE)
                .getBoolean(Constants.IS_S3_UPLOAD, false);
    }

    public void clearCredentials(Context context) {
        context.getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE)
                .edit().clear().apply();
    }

    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public void showEnableInternetDialog(final Context context) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        // set title
        alertDialogBuilder.setTitle(context.getString(R.string.title));

        // set dialog message
        alertDialogBuilder
                .setMessage(context.getString(R.string.enableNet))
                .setCancelable(false)
                .setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, close
                        // current activity
                        enableNetworkDialog.dismiss();
                        Intent intent = new Intent(Settings.ACTION_SETTINGS);
                        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        context.startActivity(intent);

                    }
                })
                .setNegativeButton(context.getString(R.string.Cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        enableNetworkDialog.dismiss();
//                        ((AppCompatActivity) context).finish();
                    }
                });

        // create alert dialog
        if (enableNetworkDialog == null || !enableNetworkDialog.isShowing())
            enableNetworkDialog = alertDialogBuilder.create();
        if (context instanceof AppCompatActivity && !((AppCompatActivity) context).isFinishing())
            enableNetworkDialog.show();
    }

    public void showLoader(Context context, String title, String content) {
        if (progressDialog == null || !progressDialog.isShowing())
            progressDialog = new ProgressDialog(context);
        if (title != null && !title.isEmpty())
            progressDialog.setTitle(title);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(content);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void closeLoader() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        progressDialog = null;
    }

    public void saveDuration(Context context, int seconds) {
        context.getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE)
                .edit()
                .putInt(Constants.USER_VIDEO_DURATION, seconds)
                .apply();
    }

    public int getLastDuration(Context context) {
        /*will return default 150 secs ie 2m 30s*/
        return context.getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE)
                .getInt(Constants.USER_VIDEO_DURATION, 150);
    }

    public static String humanReadableByteCount(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }

    public String inMinutes(int totalSecs) {
        /*int totalSecs = 150;
        int hours = totalSecs / 3600;
        int minutes = (totalSecs % 3600) / 60;
        int seconds = totalSecs % 60;

        String timeString = String.format("%02d:%02d:%02d", hours, minutes, seconds);*/
        int minutes = (totalSecs % 3600) / 60;
//        return String.format("%02d",minutes);
        return String.valueOf(minutes);

    }

    public String inSeconds(int totalSecs) {
        int seconds = totalSecs % 60;
//        return String.format("%02d",seconds);
        return String.valueOf(seconds);
    }

    /*public int getUniqueNotificId(Context context) {
        Set<String> failedVideosSet = getFailedVideoFilePathsSet(context);

        if (failedVideosSet!=null && failedVideosSet.size()>0) {
            for (String failedVid : failedVideosSet) {
                if (failedVid.contains(String.valueOf(notificationId))) {
                    String[] temp = failedVid.split("\\@");
                    notificationId = Integer.valueOf(temp[1]);
                }
            }
        }
        notificationId++;
        return notificationId;
    }*/

    public int getLatestNotificId(Context context) {
        int id = context.getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE)
                .getInt(Constants.USER_LAST_NOTIFIC_ID, 100);
        id++;

        context.getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE)
                .edit()
                .putInt(Constants.USER_LAST_NOTIFIC_ID, id)
                .apply();

        return id;
    }

    public synchronized void addFailedVideoFilePathToSet(Context context, Upload upload, int status) {
        Set<String> newUpdateStringSet = new HashSet<>();
        Set<String> updateStringSet = getFailedVideoFilePathsSet(context);  /*immutable set*/
        if (updateStringSet != null) {
            newUpdateStringSet.addAll(updateStringSet);
        }
        String failedInfo = String.valueOf(status) + "@" + upload.getNotificationId() + "@" + upload.getFilePath();

        /*removing existing failedSet*/
        Set<String> concurrentSet = newUpdateStringSet;
        for (String savedInfo : concurrentSet) {
            Log.d("failed", savedInfo);
            String[] parts = savedInfo.split("@");
            if (parts.length > 2) {
                if (parts[2].equals(upload.getFilePath())) {
                    String dummy = savedInfo;
                    newUpdateStringSet.remove(dummy);
                    Log.d("redundant", savedInfo);
                }
            }
        }

        /*Iterator<String> it = newUpdateStringSet.iterator();
        while(it.hasNext()){
            String failed = it.next();
            Log.d("failed",failed);
            String[] parts = failed.split("@");
            if (parts.length>2){
                if (parts[2].equals(upload.getFilePath())) {
                    newUpdateStringSet.remove(failed);
                    Log.d("redundant",failed);
                }
            }
        }*/

        Log.d("added", failedInfo);
        newUpdateStringSet.add(failedInfo);

        context.getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE)
                .edit()
                .putStringSet(Constants.USER_FAILED_VIDS, newUpdateStringSet)
                .apply();
    }

    public Set<String> getFailedVideoFilePathsSet(Context context) {
        return context.getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE)
                .getStringSet(Constants.USER_FAILED_VIDS, null);
    }

    public void removeFailedVideo(Context context, Upload upload) {
        Set<String> newUpdateStringSet = new HashSet<>();
        Set<String> updateStringSet = getFailedVideoFilePathsSet(context); /* <-- immutable string set*/
        if (updateStringSet == null || updateStringSet.size() == 0) {
            if (deleteFile(upload.getFilePath())) {
                deleteFileFromMediaStore(context, upload.getFilePath());
            }
            return;
        }

        newUpdateStringSet.addAll(updateStringSet);
        /*assuming that completed videos were in progress state*/
        String failedInfo = Utils.IN_PROGRESS + "@" + upload.getNotificationId() + "@" + upload.getFilePath();
        if (newUpdateStringSet.contains(failedInfo)) {
            newUpdateStringSet.remove(failedInfo);
            Log.d("removing", failedInfo);
        }

        context.getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE)
                .edit()
                .putStringSet(Constants.USER_FAILED_VIDS, newUpdateStringSet)
                .apply();

        if (deleteFile(upload.getFilePath())) {
            deleteFileFromMediaStore(context, upload.getFilePath());
        }
    }

    public boolean deleteFile(String filePath) {
        File file = new File(filePath);
        if (file.exists()) {
            boolean deleted = file.delete();
            Log.d("deleteFile", filePath + " deleted = " + deleted);
            return deleted;
        } else {
            Log.d("deleteFile", filePath + " doesn't exist");
            return false;
        }
    }

    public String getVideoNameDateFormat() throws Exception {
        Date date = new Date();
        return new SimpleDateFormat(VIDEO_NAME_DATE_FORMAT, Locale.getDefault()).format(date);
    }

    public static void deleteFileFromMediaStore(Context context, String filePath) {
        ContentResolver contentResolver = context.getContentResolver();
        File file = new File(filePath);
        String canonicalPath;
        try {
            canonicalPath = file.getCanonicalPath();
        } catch (IOException e) {
            canonicalPath = file.getAbsolutePath();
        }
        final Uri uri = MediaStore.Files.getContentUri("external");
        final int result = contentResolver.delete(uri,
                MediaStore.Files.FileColumns.DATA + "=?", new String[]{canonicalPath});
        if (result == 0) {
            final String absolutePath = file.getAbsolutePath();
            if (!absolutePath.equals(canonicalPath)) {
                contentResolver.delete(uri,
                        MediaStore.Files.FileColumns.DATA + "=?", new String[]{absolutePath});
            }
        }
    }

    /*public String getNOTIFICATION_GROUP_KEY(int uploadNotifyType) {

        switch (uploadNotifyType){
            case UPLOAD_COMPLETED_GKEY:
                return UPLOAD_COMPLETED_GK;
            case UPLOAD_FAIlED_GKEY:
                return UPLOAD_FAIlED_GK;
            case UPLOADING_GKEY:
                return UPLOADING_GK;
        }
        int key = Integer.valueOf(NOTIFICATION_GROUP_KEY);
        NOTIFICATION_GROUP_KEY = String.valueOf(key++);
        Log.d("getNOTIFICATION_KEY","key set = "+NOTIFICATION_GROUP_KEY);
        return NOTIFICATION_GROUP_KEY;
    }*/

    public String getVideoUrl(Upload upload) {
        return S3_BASE_URL
                + URL_DELIMITER
                + S3Constants.BUCKET_NAME
                + URL_DELIMITER
                + new File(upload.getFilePath()).getName();
    }

    public String getVideoFileLastModifiedTime(String videoFilePath) {
        File file = new File(videoFilePath);
        long lastModifiedMillis = file.lastModified();
        Date date = new Date(lastModifiedMillis);
        return new SimpleDateFormat(VIDEO_HEADER_DATE_FORMAT, Locale.getDefault()).format(date);
    }

    public String getVideoFileLastModifiedTime(String videoFilePath,String format) {
        File file = new File(videoFilePath);
        long lastModifiedMillis = file.lastModified();
        Date date = new Date(lastModifiedMillis);
        return new SimpleDateFormat(format, Locale.getDefault()).format(date);
    }

    /*public String getVideoFileCreattionTimeFromName(String fileName) {
        *//*filename = Amit147_22-03-2018-130839.mp4*//*
        String[] fileNameParts = fileName.split("_");
        long lastModifiedMillis = file.lastModified();
        Date date = new Date(lastModifiedMillis);
        return new SimpleDateFormat(VIDEO_HEADER_DATE_FORMAT,Locale.getDefault()).format(date);
    }*/

    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }


    private String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public List<String> getSupportedFocusModes() {
        try {
            if (supportedFocusModes == null) {
                supportedFocusModes = new ArrayList<>();

                Camera camera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
                if (camera != null) {
                    Camera.Parameters parameters = camera.getParameters();

                    supportedFocusModes = parameters.getSupportedFocusModes();
                    supportedFocusModes.add(Constants.NO_FOCUS_MODE);
                }
            }

            return supportedFocusModes;
        } catch (RuntimeException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void saveFocusMode(Context context, String mode) {
        context.getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE)
                .edit()
                .putString(Constants.USER_CAM_FOCUS_MODE, mode)
                .apply();
    }

    public String getFocusMode(Context context) {
        return context.getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE)
                .getString(Constants.USER_CAM_FOCUS_MODE, Constants.NO_FOCUS_MODE);
    }

    public void closeKeyboard(Context context, View view) {
        InputMethodManager inputManager = (InputMethodManager)
                context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow((view == null) ? null : view.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }

    public boolean videoSentToCloud(String videoUrl, Context context) {
        Set<String> videoUrlSet = context.getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE)
                .getStringSet(Constants.CLOUD_VIDEO_URLS, null);
        return !(videoUrlSet == null || videoUrlSet.size() == 0)
                && videoUrlSet.contains(videoUrl);
    }

    public void saveCloudVideoUrl(String videoUrl, Context context) {
        Set<String> newUpdateStringSet = new HashSet<>();
        Set<String> prefStringSet = context.getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE)
                .getStringSet(Constants.CLOUD_VIDEO_URLS, null);  /*immutable set*/
        if (prefStringSet != null) {
            newUpdateStringSet.addAll(prefStringSet);
        }

        if (!newUpdateStringSet.contains(videoUrl)) {
            newUpdateStringSet.add(videoUrl);
            Log.d("saveCloudVideoUrl", videoUrl + " added");
        }

        context.getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE)
                .edit()
                .putStringSet(Constants.CLOUD_VIDEO_URLS, newUpdateStringSet)
                .apply();
    }

    public boolean isCameraUsebyApp() {
        Camera camera = null;
        try {
            camera = Camera.open();
        } catch (RuntimeException e) {
            return true;
        } finally {
            if (camera != null) camera.release();
        }
        return false;
    }


    public String getFormattedDateTime(Long TimeStamp) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return formatter.format(new Date(TimeStamp));
    }
}
