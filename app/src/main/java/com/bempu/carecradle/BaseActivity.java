package com.bempu.carecradle;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class BaseActivity extends AppCompatActivity {
    private IntentFilter networkChangeIntentFilter;
    private BroadcastReceiver networkChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Constants.NETWORK_CHANGED)) {
                Utils utils = Utils.getUtils();
                if (!utils.isNetworkAvailable(context)) {
                    if (!BaseActivity.this.isFinishing())
                        utils.showEnableInternetDialog(BaseActivity.this);
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        networkChangeIntentFilter = new IntentFilter();
        networkChangeIntentFilter.addAction(Constants.NETWORK_CHANGED);
        registerReceiver(networkChangeReceiver, networkChangeIntentFilter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Utils utils = Utils.getUtils();
        if (!utils.isNetworkAvailable(BaseActivity.this)) {
            if (!BaseActivity.this.isFinishing())
                utils.showEnableInternetDialog(BaseActivity.this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            unregisterReceiver(networkChangeReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
