package com.bempu.carecradle;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bempu.carecradle.models.Mother;
import com.bempu.carecradle.models.RegisterResponse;
import com.bempu.carecradle.network.RetrofitServiceGenerator;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends /*BaseActivity*/AppCompatActivity {
    TextView msg;
    Button btn;
    private Utils utils;
    private AppCompatEditText motherName;
    private TextInputLayout motherNameInputLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        utils = Utils.getUtils();

        motherName = (AppCompatEditText) findViewById(R.id.motherNameEt);
        motherNameInputLayout = (TextInputLayout) findViewById(R.id.mother_name_input_layout);
        msg = (TextView) findViewById(R.id.info);
        btn = (Button) findViewById(R.id.registerBtn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(motherName.getText().toString())) {
//                    motherName.setError("Name cannot be empty");
                    motherNameInputLayout.setErrorEnabled(true);
                    motherNameInputLayout.setError("Name cannot be empty");
                }
                else {
                    motherNameInputLayout.setErrorEnabled(false);
                    motherNameInputLayout.setError(null);
                    registerMotherName(new Mother(motherName.getText().toString()));
                }
            }
        });
    }

    private void registerMotherName(Mother mother) {
        if (!Utils.getUtils().isNetworkAvailable(this)){
            Utils.getUtils().showEnableInternetDialog(this);
            return;
        }

        if (!isFinishing())
            utils.showLoader(RegisterActivity.this,null,"Registering mother...");
        RetrofitServiceGenerator.ApiService apiService = RetrofitServiceGenerator.createService(RetrofitServiceGenerator.ApiService.class);
        Call<RegisterResponse> call = apiService.registerMother(mother);
        call.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                if (isFinishing())
                    return;
                utils.closeLoader();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        String status = response.body().getStatus();
                        if (status.equalsIgnoreCase("success")) {
                            showPopup(response.body());
                        } else if (status.equalsIgnoreCase("error")){
                            String desc = response.body().getDescription();
                            if (desc!=null)
                                Toast.makeText(RegisterActivity.this, "Error : "+desc, Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    msg.setText(response.message());
                    Toast.makeText(RegisterActivity.this, "error : "+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                utils.closeLoader();
                msg.setText(t.getMessage());
                Toast.makeText(RegisterActivity.this, "error : "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showPopup(final RegisterResponse registerResponse){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RegisterActivity.this);
        // set dialog message
        alertDialogBuilder
                .setTitle(getString(R.string.reg_success_title))
                .setMessage(getString(R.string.popup_body)+registerResponse.getUniqueid())
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        utils.saveCredentials(RegisterActivity.this, registerResponse);
                        startActivity(new Intent(RegisterActivity.this, HomeActivity.class));
                        finish();

                    }
                })
                .create().show();
    }
}
