/*
 *  Copyright 2016 Jeroen Mols
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.bempu.carecradle;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class VideoFile {

    private static final String DIRECTORY_SEPARATOR = "/";
    private static final String DATE_FORMAT = "ddMMyyyy_HHmmss";
    private static final String DEFAULT_PREFIX = "video_";
    private static final String DEFAULT_EXTENSION = ".mp4";

    private String mFilename;
    private Date mDate;
    private Context context;

    public VideoFile(String filename) {
        this.mFilename = filename;
    }

    public VideoFile(String filename, Date date) {
        this(filename);
        this.mDate = date;
    }

    public String getFullPath() {
        return getFile().getAbsolutePath();
    }

    public File getFile() {
        final String filename = generateFilename();
        if (filename.contains(DIRECTORY_SEPARATOR)) return new File(filename);

//		final File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
        File bempuFolder = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Bempu");
        if (!bempuFolder.exists())
            bempuFolder.mkdirs();
        return new File(bempuFolder, /*generateFilename()*/filename);
    }

    private String generateFilename() {
        if (isValidFilename()) {
            Log.d("generateFilename", "Already, filename = " + mFilename);
            return mFilename;
        }

        /*final String dateStamp = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault()).format(getDate());
        return DEFAULT_PREFIX + dateStamp + DEFAULT_EXTENSION;*/
        mFilename = createFileName();
        return mFilename;
    }

    private boolean isValidFilename() {
        return mFilename!=null && !mFilename.isEmpty();
    }

    private Date getDate() {
        if (mDate == null) {
            mDate = new Date();
        }
        return mDate;
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    private String createFileName(){
        Log.d("createFileName","filename is "+mFilename);
        Utils utils = Utils.getUtils();
        String name = null;
        try {
            name = utils.getUserUniqueId(context)+"_"+utils.getVideoNameDateFormat()+DEFAULT_EXTENSION;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return name;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
