/*
 *  Copyright 2016 Jeroen Mols
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.bempu.carecradle;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore.Video.Thumbnails;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.bempu.carecradle.camera.CameraWrapper;
import com.bempu.carecradle.camera.NativeCamera;
import com.bempu.carecradle.configuration.CaptureConfiguration;
import com.bempu.carecradle.models.HardwareData;
import com.bempu.carecradle.recorder.AlreadyUsedException;
import com.bempu.carecradle.recorder.VideoRecorder;
import com.bempu.carecradle.recorder.VideoRecorderInterface;
import com.bempu.carecradle.view.RecordingButtonInterface;
import com.bempu.carecradle.view.VideoCaptureView;

import java.io.File;

import app.akexorcist.bluetotohspp.library.BluetoothSPP;

public class VideoCaptureActivity extends Activity implements RecordingButtonInterface, VideoRecorderInterface {

    public static final int RESULT_ERROR = 10101;
    private static final int REQUESTCODE_SWITCHCAMERA = 10100;

    public static final String EXTRA_OUTPUT_FILENAME = "extraoutputfilename";
    public static final String EXTRA_CAPTURE_CONFIGURATION = "extracaptureconfiguration";
    public static final String EXTRA_ERROR_MESSAGE = "extraerrormessage";

    private static final String EXTRA_FRONTFACINGCAMERASELECTED = "extracamerafacing";
    private static final String SAVED_RECORDED_BOOLEAN = "savedrecordedboolean";
    protected static final String SAVED_OUTPUT_FILENAME = "savedoutputfilename";

    private boolean mVideoRecorded = false;
    VideoFile mVideoFile = null;
    private CaptureConfiguration mCaptureConfiguration;

    private VideoCaptureView mVideoCaptureView;
    private VideoRecorder mVideoRecorder;
    private boolean isFrontFacingCameraSelected;
    private BluetoothSPP bt;
    private String TAG = VideoCaptureActivity.class.getSimpleName();

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e(TAG, intent.getStringExtra("stringvalue"));
            byte[] receivedData = intent.getByteArrayExtra("data");
            careCradleDBHelper.addSessionData(getHardwareData(receivedData));
        }
    };

    private int uniqueValue;
    private CareCradleDBHelper careCradleDBHelper;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CLog.toggleLogging(this);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_videocapture);
        uniqueValue = Utils.getUtils().getLatestNotificId(this);
        initializeCaptureConfiguration(savedInstanceState);
        careCradleDBHelper = CareCradleDBHelper.getCareCradleDBHelper(this);

        mVideoCaptureView = (VideoCaptureView) findViewById(R.id.videocapture_videocaptureview_vcv);
        if (mVideoCaptureView == null) return; // Wrong orientation

        initializeRecordingUI();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.ACTION_RECEIVE_DATA);
        registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }

    private void initializeCaptureConfiguration(final Bundle savedInstanceState) {
        mCaptureConfiguration = generateCaptureConfiguration();
        mVideoRecorded = generateVideoRecorded(savedInstanceState);
        mVideoFile = generateOutputFile(savedInstanceState);
        isFrontFacingCameraSelected = generateIsFrontFacingCameraSelected();
    }

    private void initializeRecordingUI() {
        Display display = ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
        mVideoRecorder = new VideoRecorder(this,
                mCaptureConfiguration,
                mVideoFile,
                new CameraWrapper(new NativeCamera(), display.getRotation()),
                mVideoCaptureView.getPreviewSurfaceHolder(),
                isFrontFacingCameraSelected);
        mVideoCaptureView.setRecordingButtonInterface(this);
        mVideoCaptureView.setCameraSwitchingEnabled(mCaptureConfiguration.getAllowFrontFacingCamera());
        mVideoCaptureView.setCameraFacing(isFrontFacingCameraSelected);

        if (mVideoRecorded) {
            mVideoCaptureView.updateUIRecordingFinished(getVideoThumbnail());
        } else {
            mVideoCaptureView.updateUINotRecording();
        }
        mVideoCaptureView.showTimer(mCaptureConfiguration.getShowTimer());
    }

    @Override
    protected void onPause() {
        if (mVideoRecorder != null) {
            mVideoRecorder.stopRecording(null);
        }
//        releaseAllResources();
        super.onPause();
    }

    /*disabling back button while video recording in progress*/
    @Override
    public void onBackPressed() {
//        finishCancelled();
        if (mVideoRecorded) {
            super.onBackPressed();
        }
    }

    @Override
    public void onRecordButtonClicked() {
        try {
            mVideoRecorder.toggleRecording();
        } catch (AlreadyUsedException e) {
            CLog.d(CLog.ACTIVITY, "Cannot toggle recording after cleaning up all resources");
        }
    }

    @Override
    public void onAcceptButtonClicked() {
        finishCompleted();
    }

    @Override
    public void onDeclineButtonClicked() {
        finishCancelled();
    }

    @Override
    public void onRecordingStarted() {
        mVideoCaptureView.updateUIRecordingOngoing();
    }

    @Override
    public void onSwitchCamera(boolean isFrontFacingSelected) {
        Intent intent = new Intent(VideoCaptureActivity.this, VideoCaptureActivity.class);
        intent.putExtras(getIntent().getExtras());      //Pass all the current intent parameters
        intent.putExtra(EXTRA_FRONTFACINGCAMERASELECTED, isFrontFacingSelected);
        startActivityForResult(intent, REQUESTCODE_SWITCHCAMERA);
        overridePendingTransition(R.anim.from_middle, R.anim.to_middle);
    }

    /*@Override
    public void onVideoOptionsClicked() {
        startActivity(new Intent(VideoCaptureActivity.this, SettingsActivity.class));
        finish();
        finishCancelled();
    }*/

    @Override
    public void onRecordingStopped(String message) {
        if (message != null) {
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        }

        mVideoCaptureView.updateUIRecordingFinished(getVideoThumbnail());
        releaseAllResources();
    }

    @Override
    public void onRecordingSuccess() {
        mVideoRecorded = true;
    }

    @Override
    public void onRecordingFailed(String message) {
        finishError(message);
    }

    @Override
    public void onRecordingPreviewReady() {
        /*for starting recording automatically*/
        if (mCaptureConfiguration.getMaxCaptureDuration() != -1
                && mCaptureConfiguration.getMaxCaptureDuration() > 0) {
            mVideoCaptureView.performRecordButtonClick();
        }
    }

    private void finishCompleted() {
        try {
            rescanSdcard(mVideoFile.getFullPath());
        } catch (Exception e) {
            e.printStackTrace();
        }
        startActivity(new Intent(VideoCaptureActivity.this, HomeActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .putExtra(EXTRA_OUTPUT_FILENAME, mVideoFile.getFullPath())
                .putExtra(Constants.NOTIFIC_ID, uniqueValue));
        finish();
    }

    private void finishCancelled() {
        startActivity(new Intent(VideoCaptureActivity.this, HomeActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .putExtra(EXTRA_OUTPUT_FILENAME, String.valueOf(RESULT_CANCELED)));
        finish();
    }

    private void finishError(final String message) {
        Toast.makeText(getApplicationContext(), "Can't capture video: " + message, Toast.LENGTH_LONG).show();

        startActivity(new Intent(VideoCaptureActivity.this, HomeActivity.class)
                .putExtra(EXTRA_OUTPUT_FILENAME, String.valueOf(RESULT_ERROR))
                .putExtra(EXTRA_ERROR_MESSAGE, message));
        finish();
    }

    private void releaseAllResources() {
        if (mVideoRecorder != null) {
            mVideoRecorder.releaseAllResources();
        }
    }

    protected CaptureConfiguration generateCaptureConfiguration() {
        CaptureConfiguration returnConfiguration = this.getIntent().getParcelableExtra(EXTRA_CAPTURE_CONFIGURATION);
        if (returnConfiguration == null) {
            returnConfiguration = CaptureConfiguration.getDefault();
            CLog.d(CLog.ACTIVITY, "No captureconfiguration passed - using default configuration");
        }
        return returnConfiguration;
    }

    private boolean generateVideoRecorded(final Bundle savedInstanceState) {
        if (savedInstanceState == null) return false;
        return savedInstanceState.getBoolean(SAVED_RECORDED_BOOLEAN, false);
    }

    protected VideoFile generateOutputFile(Bundle savedInstanceState) {
        VideoFile returnFile;
        returnFile = new VideoFile(this.getIntent().getStringExtra(EXTRA_OUTPUT_FILENAME));
        returnFile.setContext(this);
        // TODO: add checks to see if outputfile is writeable
        return returnFile;
    }

    private boolean generateIsFrontFacingCameraSelected() {
        return getIntent().getBooleanExtra(EXTRA_FRONTFACINGCAMERASELECTED, false);
    }

    public Bitmap getVideoThumbnail() {
        final Bitmap thumbnail = ThumbnailUtils.createVideoThumbnail(mVideoFile.getFullPath(),
                Thumbnails.FULL_SCREEN_KIND);
        if (thumbnail == null) {
            CLog.d(CLog.ACTIVITY, "Failed to generate video preview");
        }
        return thumbnail;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        this.setResult(resultCode, data);
        finish();
    }

    private void rescanSdcard(String filePath) throws Exception {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);

            Uri contentUri = Uri.fromFile(new File(filePath));
//            out is your output file
            mediaScanIntent.setData(contentUri);
            sendBroadcast(mediaScanIntent);
        } else {
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
                    Uri.parse("file://" + Environment.getExternalStorageDirectory())));
        }
    }

    /*disabling home button while video recording in progress*/
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_HOME) {
            Log.d("onKeyDown", "Home button pressed");
        }

        return false;
    }

    private HardwareData getHardwareData(byte[] data) {
        HardwareData hardwareData = new HardwareData();
        int status = data[0] & (0Xff);
        int heartRate = data[1] & (0Xff);
        int spo2 = data[2] & (0Xff);
        int batteryRemaning = data[3] & (0Xff);

        hardwareData.setTime(System.currentTimeMillis() + "");
        hardwareData.setBattery(batteryRemaning);
        hardwareData.setHr(heartRate + "");
        hardwareData.setStatus(status + "");
        hardwareData.setSpo2(spo2 + "");
        hardwareData.setVideoFilePath(mVideoFile.getFullPath());
        return hardwareData;
    }
}
