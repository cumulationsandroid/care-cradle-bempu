package com.bempu.carecradle;

/**
 * Created by Amit T on 21-09-2017.
 */

public class Constants {
    public final static String USER_PREF = "userPref";
    public static final String USER_UNIQUE_ID = "userUniqueId";
    public static final String USER_NAME = "userName";
    public static final String USER_API_KEY = "userApiKey";
    public static final String HEADER_API_KEY = "Api-Key";
    public static final String HEADER_UID = "uid";
    public static final String HEADER_URL = "url";
    public static final String HEADER_TIME = "time";
    public static final String FILE_PATH = "filePath";
    public static final String MESSAGE_PROGRESS = "message_progress";
    public static final String UPLOAD = "upload";
    public static final String LOAD_FRAGMENT = "loadFragment";
    public static final String NETWORK_CHANGED = "networkChanged";
    public static final String USER_VIDEO_DURATION = "userVideoDuration";
    public static final String USER_LAST_NOTIFIC_ID = "userLastNotificId";
    public static final String NOTIFIC_ID = "notificId";
    public static final String USER_FAILED_VIDS = "userFailedVids";
    public static final String USER_SETTINGS_PWD = "userSettingsPwd";
    public static final String USER_DEACTIVATE = "userDeactivate";
    public static final String HEADER_NAME = "vname";
    public static final String IS_S3_UPLOAD = "isS3Upload";
    public static final String NO_FOCUS_MODE = "no-focus";
    public static final String USER_CAM_FOCUS_MODE = "userCamFocusMode";
    public static final String UPLOAD_MSG = "uploadMsg";
    public static final String CLOUD_VIDEO_URLS = "cloudVideoUrls";
    public static final String BLUETOOTH_DEVICE_ADDRESS="bluetooth_device_address";
    public static final String DEVICE_CONNECTION_STATE="device_connect_state";
    public static final String ACTION_RECEIVE_DATA="action_receive_data";
}
