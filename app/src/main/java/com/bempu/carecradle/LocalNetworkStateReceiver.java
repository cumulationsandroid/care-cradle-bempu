package com.bempu.carecradle;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class LocalNetworkStateReceiver extends BroadcastReceiver {

    private static final String TAG = "LocalNetworkStateReceiver";

    @Override
    public void onReceive(final Context context, Intent intent) {
        if (intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")){
            Intent broadcastIntent1 = new Intent(Constants.NETWORK_CHANGED);
            context.sendBroadcast(broadcastIntent1);
        }
    }

}