package com.bempu.carecradle.network;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.text.format.Formatter;
import android.util.Log;
import android.widget.Toast;

import com.bempu.carecradle.CareCradleDBHelper;
import com.bempu.carecradle.Constants;
import com.bempu.carecradle.NotificationUtils;
import com.bempu.carecradle.RegisterActivity;
import com.bempu.carecradle.Utils;
import com.bempu.carecradle.models.DeviceDataRequest;
import com.bempu.carecradle.models.DeviceDataResponse;
import com.bempu.carecradle.models.FailedUploadEvent;
import com.bempu.carecradle.models.HardwareData;
import com.bempu.carecradle.models.Upload;
import com.bempu.carecradle.models.UploadResponse;
import com.bempu.carecradle.s3uploads.S3EventUtil;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okio.BufferedSink;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UploadService extends Service {
    private NotificationUtils notificationUtils;
    private Upload currentUpload;
    private CareCradleDBHelper careCradleDBHelper;

    @Override
    public void onCreate() {
        super.onCreate();

        careCradleDBHelper = CareCradleDBHelper.getCareCradleDBHelper(this);
        notificationUtils = NotificationUtils.getNotificationUtils();
        notificationUtils.build(this);
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        String filePath;
        if (intent != null) {
        /*make service run every 5 mins*/
            setRecurringAlarm(this);
            filePath = intent.getStringExtra(Constants.FILE_PATH);
            int uniqueId = intent.getIntExtra(Constants.NOTIFIC_ID, 100);

            if (filePath == null || uniqueId == 0) {
                Log.d("onStartCommand", "empty intent");
                uploadFailedVideos();
            } else {
                Upload upload = new Upload();
                upload.setFilePath(filePath);
                upload.setNotificationId(uniqueId);
                sendBtSessionData(upload);
                initUpload(upload);
            }
        }
        return START_STICKY;
    }

    private void uploadFailedVideos() {
        List<Upload> uploadList = careCradleDBHelper.getFailedVideos();
        if (uploadList == null || uploadList.size() == 0) {
            Log.d("uploadFailedVideos", "zero failed videos");
//            stopSelf();
            return;
        }

        for (Upload upload : uploadList) {
            initUpload(upload);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void initUpload(final Upload upload) {

        if (upload.getFilePath() == null) return;

        currentUpload = upload;
        notificationUtils.showNotification(upload, NotificationUtils.NotificationType.UPLOAD_STARTED);
        sendIntent(upload, NotificationUtils.NotificationType.UPLOAD_STARTED.toString());
        careCradleDBHelper.addVideoToDb(upload, Upload.UPLOADING);

        final File videoFile = new File(upload.getFilePath());
        if (!videoFile.exists()) {
            Log.d("initUpload", "video file not found = " + videoFile.getAbsolutePath());
            notificationUtils.showNotification(upload, NotificationUtils.NotificationType.UPLOAD_FILE_NOT_FOUND);
            sendIntent(upload, NotificationUtils.NotificationType.UPLOAD_FILE_NOT_FOUND.toString());
            careCradleDBHelper.updateVideoStatusToDb(upload, Upload.DELETED);
            return;
        }

        /*For sending event to fabrics in case of failed uploads*/
        final FailedUploadEvent failedUploadEvent = new FailedUploadEvent();
        failedUploadEvent.setuId(Utils.getUtils().getUserUniqueId(this));
        failedUploadEvent.setModel(Utils.getUtils().getDeviceName());
        failedUploadEvent.setNetworkType(S3EventUtil.getS3EventUtil().getNetworkClass(this));
        failedUploadEvent.setName(new File(upload.getFilePath()).getName());
        failedUploadEvent.setCarrier(S3EventUtil.getS3EventUtil().getPrimarySimOperatorName(this));
        long bytesTotal = new File(upload.getFilePath()).length();
        failedUploadEvent.setSize(Formatter.formatFileSize(UploadService.this, bytesTotal));
        failedUploadEvent.setUploadTime(S3EventUtil.getS3EventUtil().getCurrentTime());

        ProgressRequestBody progressRequestBody = new ProgressRequestBody(upload);
        MultipartBody.Part vFile = MultipartBody.Part.createFormData("file", videoFile.getName(), /*videoBody*/progressRequestBody);

        Utils utils = Utils.getUtils();
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put(Constants.HEADER_API_KEY, utils.getUserApiKey(this));
        headerMap.put(Constants.HEADER_UID, utils.getUserUniqueId(this));
        headerMap.put(Constants.HEADER_NAME, videoFile.getName()/*.replace(".mp4","")*/);

        RetrofitServiceGenerator.ApiService apiService = RetrofitServiceGenerator.createService(RetrofitServiceGenerator.ApiService.class);
        Call<UploadResponse> call = apiService.uploadVideoFile(vFile, headerMap);

        call.enqueue(new Callback<UploadResponse>() {
            @Override
            public void onResponse(Call<UploadResponse> call, Response<UploadResponse> response) {
                Log.d("onResponse", "success");
                if (!response.isSuccessful()) {
                    Log.d("uploadError", "msg = " + response.message());

                    String currentSize = Formatter.formatFileSize(UploadService.this, upload.getCurrentFileSize());
                    failedUploadEvent.setUploadPercent(String.valueOf(upload.getProgress()));
                    failedUploadEvent.setBytesFailed(currentSize);
                    failedUploadEvent.setFailureTime(S3EventUtil.getS3EventUtil().getCurrentTime());
                    failedUploadEvent.setFailure(response.message() + "_" + failedUploadEvent.getFailureTime());
                    logEventToFabrics(failedUploadEvent);

                    upload.setProgress(0);
                    notificationUtils.showNotification(upload, NotificationUtils.NotificationType.UPLOAD_FAILED_NETWORK);
                    sendIntent(upload, NotificationUtils.NotificationType.UPLOAD_FAILED_NETWORK.toString());
                    careCradleDBHelper.updateVideoStatusToDb(upload, Upload.FAILED);
                } else {
                    UploadResponse uploadResponse = response.body();
                    if (uploadResponse.getStatus().equalsIgnoreCase("success")) {
                        upload.setProgress(100);
                        notificationUtils.showNotification(upload, NotificationUtils.NotificationType.UPLOAD_COMPLETED);
                        sendIntent(upload, NotificationUtils.NotificationType.UPLOAD_COMPLETED.toString());
                        addVideoUrlToCloud(upload,false);
                    } else if (uploadResponse.getStatus().equalsIgnoreCase("error")) {

                        String desc = uploadResponse.getDescription();

                        String currentSize = Formatter.formatFileSize(UploadService.this, upload.getCurrentFileSize());
                        failedUploadEvent.setUploadPercent(String.valueOf(upload.getProgress()));
                        failedUploadEvent.setBytesFailed(currentSize);
                        failedUploadEvent.setFailureTime(S3EventUtil.getS3EventUtil().getCurrentTime());
                        failedUploadEvent.setFailure(desc + "_" + failedUploadEvent.getFailureTime());
                        logEventToFabrics(failedUploadEvent);

                        upload.setProgress(0);
                        Toast.makeText(UploadService.this, "Error : " + desc, Toast.LENGTH_SHORT).show();

                        notificationUtils.showNotification(upload, NotificationUtils.NotificationType.UPLOAD_FAILED_NETWORK);
                        sendIntent(upload, NotificationUtils.NotificationType.UPLOAD_FAILED_NETWORK.toString());

                        careCradleDBHelper.updateVideoStatusToDb(upload, Upload.FAILED);
                    }
                }
            }

            @Override
            public void onFailure(Call<UploadResponse> call, Throwable t) {
                Log.d("onFailure", "error = " + t.getMessage());
                t.printStackTrace();

                String currentSize = Formatter.formatFileSize(UploadService.this, upload.getCurrentFileSize());
                failedUploadEvent.setUploadPercent(String.valueOf(upload.getProgress()));
                failedUploadEvent.setBytesFailed(currentSize);
                failedUploadEvent.setFailureTime(S3EventUtil.getS3EventUtil().getCurrentTime());
                failedUploadEvent.setFailure(t.getMessage() + "_" + failedUploadEvent.getFailureTime());
                logEventToFabrics(failedUploadEvent);

                upload.setProgress(0);
                notificationUtils.showNotification(upload, NotificationUtils.NotificationType.UPLOAD_FAILED_NETWORK);
                sendIntent(upload, NotificationUtils.NotificationType.UPLOAD_FAILED_NETWORK.toString());
                careCradleDBHelper.updateVideoStatusToDb(upload, Upload.FAILED);
            }
        });
    }

    private void sendNotification(Upload upload) {
        notificationUtils.showNotification(upload, NotificationUtils.NotificationType.UPLOADING);
        sendIntent(upload, NotificationUtils.NotificationType.UPLOADING.toString());
    }

    private void sendIntent(Upload upload, String uploadMsg) {

        Intent intent = new Intent(Constants.MESSAGE_PROGRESS);
        intent.putExtra(Constants.UPLOAD, upload);
        intent.putExtra(Constants.UPLOAD_MSG, uploadMsg);
        LocalBroadcastManager.getInstance(UploadService.this).sendBroadcast(intent);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
//        notificationManager.cancel(rootIntent.getIntExtra(Constants.NOTIFIC_ID,0));
        if (currentUpload == null)
            return;
        notificationUtils.clearNotification(currentUpload.getNotificationId());
        if (currentUpload.getNotificationId() > 0
                && currentUpload.getFilePath() != null
                && currentUpload.getProgress() < 100)
        {
            careCradleDBHelper.updateVideoStatusToDb(currentUpload, Upload.FAILED);
        }
    }

    private class ProgressRequestBody extends RequestBody {
        private File mFile;
        //        private UploadCallbacks mListener;
        private final int DEFAULT_BUFFER_SIZE = 1024 * 36; /*36 KB*/
        private Upload upload;

        public ProgressRequestBody(Upload upload/*,final UploadCallbacks listener*/) {
//            mListener = listener;
            this.upload = upload;
            mFile = new File(upload.getFilePath());
        }

        @Override
        public MediaType contentType() {
            // i want to upload only videos
            return MediaType.parse("video/*");
        }

        @Override
        public long contentLength() throws IOException {
            return mFile.length();
        }

        @Override
        public void writeTo(BufferedSink sink) throws IOException {
            long fileLength = mFile.length();
            byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
            FileInputStream in = new FileInputStream(mFile);
            long uploaded = 0;
            long startTime = System.currentTimeMillis();
            int timeCount = 1;

            try {
                int read;
//                Handler handler = new Handler(Looper.getMainLooper());
                while ((read = in.read(buffer)) != -1) {

                    // update progress on UI thread
//                    handler.post(new ProgressUpdater(uploaded, fileLength));

                    uploaded += read;
                    int totalFileSize = (int) (fileLength);
                    long currentTime = System.currentTimeMillis() - startTime;

                    float percentage = (((float) uploaded / (float) totalFileSize) * 100);
                    upload.setTotalFileSize(totalFileSize);

                    Log.d("UploadService", "notification perc = " + percentage);

                    /*Will update notification every 0.5 second*/
                    if (currentTime > 500 * timeCount) {

                        upload.setCurrentFileSize((int) uploaded);
                        upload.setProgress((int) percentage);
                        sendNotification(upload);
                        timeCount++;
                    }
                    sink.write(buffer, 0, read);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                Log.d("writeTo", "finally called");
                in.close();
            }
        }

        /*private class ProgressUpdater implements Runnable {
            private long mUploaded;
            private long mTotal;

            public ProgressUpdater(long uploaded, long total) {
                mUploaded = uploaded;
                mTotal = total;
            }

            @Override
            public void run() {
                mListener.onProgressUpdate((int) (100 * mUploaded / mTotal));
            }
        }*/
    }

    /*public interface UploadCallbacks {
        void onProgressUpdate(int percentage);

        void onError();

        void onFinish();
    }*/

    private void setRecurringAlarm(Context context) {
        /*Calendar updateTime = Calendar.getInstance();
        updateTime.setTimeZone(TimeZone.getDefault());
        updateTime.set(Calendar.HOUR_OF_DAY, 20);
        updateTime.set(Calendar.MINUTE, 30);*/

        Intent uploadServicw = new Intent(context, UploadService.class);
        uploadServicw.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getService(context, 0, uploadServicw, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        // should be AlarmManager.INTERVAL_DAY (but changed to 15min for testing)
        long triggerAtMillis = System.currentTimeMillis() + 300000;
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, triggerAtMillis,
                /*AlarmManager.INTERVAL_FIFTEEN_MINUTES*/300000, pendingIntent);
        Log.d("UploadService", "Set alarmManager.setRepeating to: " + System.currentTimeMillis());
    }

    private void logEventToFabrics(FailedUploadEvent failedUploadEvent) {
        try {
            Answers.getInstance().logCustom(new CustomEvent("Failed Uploads")
                    .putCustomAttribute("User ID", failedUploadEvent.getuId())
                    .putCustomAttribute("Device Model", failedUploadEvent.getModel())
                    .putCustomAttribute("Name", failedUploadEvent.getName())
                    .putCustomAttribute("File Size", failedUploadEvent.getSize())
                    .putCustomAttribute("Upload Time", failedUploadEvent.getUploadTime())
                    .putCustomAttribute("Network Type", failedUploadEvent.getNetworkType())
                    .putCustomAttribute("Carrier", failedUploadEvent.getCarrier())
                    .putCustomAttribute("Upload Percent", failedUploadEvent.getUploadPercent())
                    .putCustomAttribute("Failure Reason", failedUploadEvent.getFailure())
                    .putCustomAttribute("Failure Time", failedUploadEvent.getFailureTime())
                    .putCustomAttribute("Size Failed", failedUploadEvent.getBytesFailed())
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addVideoUrlToCloud(final Upload upload, final boolean emptyUrl) {

        final Utils utils = Utils.getUtils();

        final String videoUrl = utils.getVideoUrl(upload);

        if (utils.videoSentToCloud(videoUrl, this))
            return;

        if (!utils.isNetworkAvailable(this)) {
            Toast.makeText(this, "Videos Uploaded : Error, check network connection", Toast.LENGTH_SHORT).show();
            careCradleDBHelper.updateVideoStatusToDb(upload, Upload.FAILED);
            return;
        }

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put(Constants.HEADER_API_KEY, utils.getUserApiKey(this));
        headerMap.put(Constants.HEADER_UID, utils.getUserUniqueId(this));
        if (!emptyUrl)
            headerMap.put(Constants.HEADER_URL, videoUrl);
        else
            headerMap.put(Constants.HEADER_URL, "");
        headerMap.put(Constants.HEADER_TIME, utils.getVideoFileLastModifiedTime(upload.getFilePath()));

        RetrofitServiceGenerator.ApiService apiService = RetrofitServiceGenerator.createService(RetrofitServiceGenerator.ApiService.class);
        Call<UploadResponse> call = apiService.addVideoUrl(headerMap);

        call.enqueue(new Callback<UploadResponse>() {
            @Override
            public void onResponse(Call<UploadResponse> call, Response<UploadResponse> response) {
                if (response.isSuccessful()) {
                    String status = response.body().getStatus();
                    if (status.equals("success")) {
                        Log.d("addVideoUrl", "success url = " + response.body().getUrl());
//                        sendBtSessionData(upload);
                        if (!emptyUrl)
                            updateVideoCompletedStatusToDb(upload);
                    } else if (status.trim().equalsIgnoreCase("data are repeating")) {
//                        sendBtSessionData(upload);
                        if (!emptyUrl)
                            updateVideoCompletedStatusToDb(upload);
                    } else if (status.equals("error")) {
                        String desc = response.body().getDescription();
                        Log.e("addVideoUrl", "error = " + desc);
                        if (response.message().equalsIgnoreCase("Unauthorized")) {
                            Utils.getUtils().clearCredentials(UploadService.this);
                            startActivity(new Intent(UploadService.this, RegisterActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                        }
                    }
                } else {
                    Log.e("addVideoUrl", "error = " + response.message());
                }
            }

            @Override
            public void onFailure(Call<UploadResponse> call, Throwable t) {
                t.printStackTrace();
                Log.e("addVideoUrl", "error = " + t.getMessage());
                careCradleDBHelper.updateVideoStatusToDb(upload, Upload.FAILED);
            }
        });
    }

    private void updateVideoCompletedStatusToDb(Upload upload) {
        careCradleDBHelper.updateVideoStatusToDb(upload, Upload.COMPLETED);
        Utils.getUtils().saveCloudVideoUrl(Utils.getUtils().getVideoUrl(upload), UploadService.this);
        if (Utils.getUtils().deleteFile(upload.getFilePath()))
            Utils.deleteFileFromMediaStore(UploadService.this, upload.getFilePath());
    }

    private void sendBtSessionData(final Upload upload) {
        List<HardwareData> hardwareList = careCradleDBHelper.getDeviceDataListForVideo(upload.getFilePath());
        if (hardwareList.size() == 0)
            return;

        DeviceDataRequest deviceDataRequest = new DeviceDataRequest();

        deviceDataRequest.setUniqueid(Utils.getUtils().getUserUniqueId(this));
        deviceDataRequest.setR_time(Utils.getUtils().getVideoFileLastModifiedTime(upload.getFilePath(),"yyyy-MM-dd HH:mm:ss"));
        deviceDataRequest.setHardware_data(hardwareList);

        RetrofitServiceGenerator.ApiService apiService = RetrofitServiceGenerator.createService(RetrofitServiceGenerator.ApiService.class);
        Call<DeviceDataResponse> call = apiService.sendDeviceData(deviceDataRequest);

        call.enqueue(new Callback<DeviceDataResponse>() {
            @Override
            public void onResponse(Call<DeviceDataResponse> call, Response<DeviceDataResponse> response) {
                if (response!=null && response.body()!=null){
                    String status = response.body().getStatus();
                    if (status!=null && status.equalsIgnoreCase("success")){
                        Log.d("sendBtSessionData","success");
                        int count = careCradleDBHelper.clearSessionTable();
                        Log.d("sendBtSessionData","session table cleared = "+count);
                        addVideoUrlToCloud(upload,true);
                    } else {
                        String cMsg = response.body().getCustom_message();
//                        Toast.makeText(UploadService.this, ""+cMsg, Toast.LENGTH_SHORT).show();
                        Log.d("sendBtSessionData",cMsg);
                    }
                }
            }

            @Override
            public void onFailure(Call<DeviceDataResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });


    }
}