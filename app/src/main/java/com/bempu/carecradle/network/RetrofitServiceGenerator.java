package com.bempu.carecradle.network;

import com.bempu.carecradle.BuildConfig;
import com.bempu.carecradle.models.DeactivateResponse;
import com.bempu.carecradle.models.DeviceDataRequest;
import com.bempu.carecradle.models.DeviceDataResponse;
import com.bempu.carecradle.models.GetStatusResponse;
import com.bempu.carecradle.models.Mother;
import com.bempu.carecradle.models.RegisterResponse;
import com.bempu.carecradle.models.UploadResponse;
import com.bempu.carecradle.models.VideoCountResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Streaming;

public class RetrofitServiceGenerator {

    public static final String PROD_BASE_URL /*= "http://35.165.93.101/bempu/"*/
                                            /*= "http://35.167.76.35/bempu/";*/
            = "http://52.11.135.172/";

    public static final String DEV_BASE_URL = "http://35.167.76.35/bempu/";

    private static Gson gson = new GsonBuilder()
            .setLenient()
            .create();

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(PROD_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson));

    public static <S> S createService(Class<S> serviceClass) {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        /*Enable logging only for debug, else http throws out of memory error crash for large files to log
        * Also, when enabled, ProgressRequestBody writTo method gets called twice because of Http logging*/
        if (BuildConfig.DEBUG)
            logging.setLevel(HttpLoggingInterceptor.Level.NONE);
        else logging.setLevel(HttpLoggingInterceptor.Level.NONE);
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.MINUTES)
                .writeTimeout(10, TimeUnit.MINUTES)
                .readTimeout(10, TimeUnit.MINUTES)
                .addInterceptor(logging)
                .build();

        builder.client(okHttpClient);
        Retrofit retrofit = builder.build();
        return retrofit.create(serviceClass);
    }

    public interface ApiService {
        @POST("app/register-mother")
        Call<RegisterResponse> registerMother(@Body Mother mother);

        /*@Multipart
        @POST("app/upload-video")
        Call<ResponseBody> uploadVideoFile(@Part MultipartBody.Part video,
                                           @Part("file") RequestBody fileName,
                                           @Part("uniqueid") RequestBody uniqueid);*/

        @Streaming
        @Multipart
        @POST("app/upload-video")
        Call<UploadResponse> uploadVideoFile(@Part MultipartBody.Part video,
                                             @HeaderMap Map<String, String> headers);

        @POST("app/deactivate")
        Call<DeactivateResponse> deactivateUser(@HeaderMap Map<String, String> headers);

        @GET("app/view")
        Call<VideoCountResponse> getVidsCount(@HeaderMap Map<String, String> headers);

        @POST("app/add-video")
        Call<UploadResponse> addVideoUrl(@HeaderMap Map<String, String> headers);

        @POST("app/get-status")
        Call<GetStatusResponse> getUserStatus(@HeaderMap Map<String, String> headers);

        @POST("app/device-data")
        Call<DeviceDataResponse> sendDeviceData(@Body DeviceDataRequest deviceDataRequest);
    }
}