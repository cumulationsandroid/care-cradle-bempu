package com.bempu.carecradle;

import android.content.Intent;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.LoginEvent;

import io.fabric.sdk.android.Fabric;

import java.util.concurrent.TimeUnit;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Don't do this! This is just so cold launches take some time
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (Utils.getUtils().getUserUniqueId(SplashScreenActivity.this).isEmpty()) {
                    Intent intent = new Intent(SplashScreenActivity.this, RegisterActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Crashlytics.setUserIdentifier(Utils.getUtils().getUserUniqueId(SplashScreenActivity.this));
                    Answers.getInstance().logLogin(new LoginEvent()
                            .putMethod("Mother")
                            .putSuccess(true)
                            .putCustomAttribute("userID", Utils.getUtils().getUserUniqueId(SplashScreenActivity.this)));
                    startActivity(new Intent(SplashScreenActivity.this, HomeActivity.class));
                    finish();
                }
            }
        }, 2000);
    }
}
