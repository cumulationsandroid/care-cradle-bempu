package com.bempu.carecradle;

import android.content.Context;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.text.format.Formatter;

import com.bempu.carecradle.models.Upload;

import java.io.File;

/**
 * Created by Amit T on 30-10-2017.
 */

public class NotificationUtils {
    private NotificationManagerCompat notificationManager;
    private NotificationCompat.Builder notificationBuilder;
    private static NotificationUtils notificationUtils;
    private Context context;

    private NotificationUtils() {
    }

    public static NotificationUtils getNotificationUtils(){
        if (notificationUtils == null)
            notificationUtils = new NotificationUtils();
        return notificationUtils;
    }

    public void build(Context context){
        this.context = context;
        if (notificationManager == null)
            notificationManager = NotificationManagerCompat.from(context);
        if (notificationBuilder == null)
            createNotificationBuilder(context);
    }

    public void showNotification(Upload upload,NotificationType type){
        File videoFile = new File(upload.getFilePath());

        switch (type){
            case UPLOAD_STARTED:
                notificationManager.cancel(/*0*/upload.getNotificationId());
                notificationBuilder.setContentTitle("Upload Started");
                notificationBuilder.setContentText(videoFile.getName()+" uploading started..");
                notificationBuilder.setStyle(new NotificationCompat.BigTextStyle()
                    .bigText(videoFile.getName()+" uploading started.."));
                break;
            case UPLOADING:
                String currentSize = Formatter.formatFileSize(context, upload.getCurrentFileSize());
                String totalSize = Formatter.formatFileSize(context, upload.getTotalFileSize());
//                notificationManager.cancel(/*0*/upload.getNotificationId());
                notificationBuilder.setProgress(100, upload.getProgress(), false);
                notificationBuilder.setContentTitle("Uploading video");
                notificationBuilder.setContentText(currentSize + "/" + totalSize+" done.");
                notificationBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(currentSize + "/" + totalSize+" done."));
                break;
            case UPLOAD_COMPLETED:
                notificationManager.cancel(/*0*/upload.getNotificationId());
                notificationBuilder.setProgress(0, 0, false);
                notificationBuilder.setContentTitle("Upload successful");
                notificationBuilder.setContentText(videoFile.getName()+" upload completed");
                notificationBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(videoFile.getName()+" upload completed"));
                break;
            case UPLOAD_FILE_NOT_FOUND:
                notificationManager.cancel(/*0*/upload.getNotificationId());
                notificationBuilder.setProgress(0, 0, false);
                notificationBuilder.setContentTitle("Upload failure");
                notificationBuilder.setContentText(videoFile.getName()+" video file not found");
                notificationBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(videoFile.getName()+" video file not found"));
                break;
            /*case UPLOAD_FAILED_NETWORK:
                notificationManager.cancel(*//*0*//*upload.getNotificationId());
                notificationBuilder.setProgress(0, 0, false);
                notificationBuilder.setContentTitle("Upload failure");
                notificationBuilder.setContentText("Video : "+videoFile.getName()+", network error.");
				notificationBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText("Video : "+videoFile.getName()+", network error."));
                break;*/

            case UPLOAD_FAILED:
            case UPLOAD_FAILED_NETWORK:
                notificationManager.cancel(/*0*/upload.getNotificationId());
                notificationBuilder.setProgress(0, 0, false);
                notificationBuilder.setContentTitle("Upload failure");
                notificationBuilder.setContentText("Bad network, "+videoFile.getName()+" uploading to server failed");
                notificationBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText("Bad network, "+videoFile.getName()+" uploading to server failed"));
                break;

            case UPLOAD_CANCELLED:
                notificationManager.cancel(/*0*/upload.getNotificationId());
                notificationBuilder.setProgress(0, 0, false);
                notificationBuilder.setContentTitle("Upload failure");
                notificationBuilder.setContentText(videoFile.getName()+" Uploading to server failed");
                notificationBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(videoFile.getName()+" uploading to server cancelled"));
                break;
            case UPLOAD_WAITING_FOR_NETWORK:
                notificationManager.cancel(/*0*/upload.getNotificationId());
                notificationBuilder.setProgress(0, 0, false);
                notificationBuilder.setContentTitle("Upload Waiting");
                notificationBuilder.setContentText(videoFile.getName()+" waiting for network.");
                notificationBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(videoFile.getName()+" waiting for network."));
                break;

        }

        /*To remove time stamp in Android <17*/
        if (Build.VERSION.SDK_INT<17)
            notificationBuilder.setWhen(0);
        else
            notificationBuilder.setWhen(System.currentTimeMillis());
        notificationBuilder.setShowWhen(false);
        notificationManager.notify(upload.getNotificationId(), notificationBuilder.build());
    }

    public enum NotificationType{
        UPLOAD_STARTED,
        UPLOADING,
        UPLOAD_COMPLETED,
        UPLOAD_FAILED,
        UPLOAD_FAILED_NETWORK,
        UPLOAD_WAITING_FOR_NETWORK,
        UPLOAD_FILE_NOT_FOUND,
        UPLOAD_CANCELLED
    }

    private /*NotificationCompat.Builder*/void createNotificationBuilder(Context context){
        /*return*/ notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(getSmallIcon())
                .setContentTitle("Video Upload")
                .setContentText("Uploading video.....")
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText("Uploading video....."))
                .setAutoCancel(true);
        /*To remove time stamp in Android <17*/
        if (Build.VERSION.SDK_INT<17)
            notificationBuilder.setWhen(0);
        else
            notificationBuilder.setWhen(System.currentTimeMillis());
        notificationBuilder.setShowWhen(false);
    }

    private int getSmallIcon(){
        return (Build.VERSION.SDK_INT>Build.VERSION_CODES.LOLLIPOP)
                ? R.mipmap.ic_stat_launcher : R.mipmap.ic_launcher;
    }

    /*private int getSmallIcon(){
        int API_VERSION = Build.VERSION.SDK_INT;
        int iconId;

        if (API_VERSION>Build.VERSION_CODES.LOLLIPOP){
            iconId = R.mipmap.ic_stat_launcher;
        } else {
            iconId = R.mipmap.ic_launcher;
        }

        return iconId;
    }*/

    public void clearNotification(int notificId){
        notificationManager.cancel(notificId);
    }
}
