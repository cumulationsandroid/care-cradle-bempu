package com.bempu.carecradle;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.Formatter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.bempu.carecradle.configuration.CaptureConfiguration;
import com.bempu.carecradle.configuration.PredefinedCaptureConfigurations;
import com.bempu.carecradle.models.DeviceDataRequest;
import com.bempu.carecradle.models.DeviceDataResponse;
import com.bempu.carecradle.models.GetStatusResponse;
import com.bempu.carecradle.models.HardwareData;
import com.bempu.carecradle.models.Upload;
import com.bempu.carecradle.network.RetrofitServiceGenerator;
import com.bempu.carecradle.network.UploadService;
import com.bempu.carecradle.s3uploads.S3UploadService;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import app.akexorcist.bluetotohspp.library.BluetoothSPP;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static app.akexorcist.bluetotohspp.library.BluetoothState.REQUEST_ENABLE_BT;
import static com.bempu.carecradle.Constants.MESSAGE_PROGRESS;
import static com.bempu.carecradle.VideoCaptureActivity.EXTRA_OUTPUT_FILENAME;
import static com.bempu.carecradle.VideoCaptureActivity.RESULT_ERROR;

public class HomeActivity extends /*BaseActivity*/AppCompatActivity implements View.OnClickListener {
    private String statusMessage = null;
    private String filePath = null;

    private TextView statusTv;

    private String name;

    private LinearLayout uploadScrollLayout;
    private LinearLayout.LayoutParams progressLayoutParams;
    private static final int TIME_INTERVAL = 2000; // # milliseconds, desired time passed between two back presses.
    private long mBackPressedInterval;
    private Toast backToast;
    private BluetoothSPP bt;
    private ProgressDialog progressDialog;
    private SharedPreferences sharedPreferences;
    private int LOCATION_REQUEST_CODE = 1010;
    private Button captureBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        BTinitialization();

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        myToolbar.setTitle("Home");

        captureBtn = (Button) findViewById(R.id.startVideo);
        captureBtn.setOnClickListener(this);
        captureBtn.setSelected(false);

        statusTv = (TextView) findViewById(R.id.tv_status);

        uploadScrollLayout = (LinearLayout) findViewById(R.id.upload_scroll_layout);
        progressLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        name = Utils.getUtils().getUserUniqueId(this);
        statusMessage = "Mother : " + name + "\n";

        if (getIntent() != null && getIntent().getStringExtra(EXTRA_OUTPUT_FILENAME) != null) {

            /*For handling Can't capture video: Camera disabled or is used by some other process*/
            if (getIntent().getStringExtra(EXTRA_OUTPUT_FILENAME).equals(String.valueOf(RESULT_ERROR))) {
                captureBtn.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        captureBtn.performClick();
                    }
                }, 1500);

                return;
            }

            filePath = getIntent().getStringExtra(EXTRA_OUTPUT_FILENAME);
            statusMessage = "Mother : " + name + "\n" + String.format(getString(R.string.status_capturesuccess), filePath);
            if (filePath != null)
                startUpload(filePath);
        }

        if (filePath == null) {
            if (!Utils.getUtils().isS3Upload(this)) {
                /*Don't start this service at all for s3*/
                startService(new Intent(this, UploadService.class));
            } else {
//                startService(new Intent(this, S3UploadService.class));
            }
        }

        registerReceiver();
        updateStatusAndThumbnail();
        SettingsActivity.selectedFocusMode = Utils.getUtils().getFocusMode(this);
        backToast = Toast.makeText(this, R.string.close_app_alert, Toast.LENGTH_SHORT);
    }

    private void BTinitialization() {
        bt = new BluetoothSPP(this);
        setUpBTConnectionLisenter();
        setUPDataReceiver();
    }

    @Override
    protected void onStart() {
        super.onStart();
        getMotherStatus();
        initiBlueToothService();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.startVideo:
                captureBtn.setSelected(true);
                sycnWithDevice();
                break;
        }
    }

    private void sycnWithDevice() {
        if (sharedPreferences.getBoolean(Constants.DEVICE_CONNECTION_STATE, false)
                && !sharedPreferences.getString(Constants.BLUETOOTH_DEVICE_ADDRESS, "").isEmpty()) {
            checkForPermission();
        } else {
            startVideoCaptureActivity();
        }
    }

    private void startVideoCaptureActivity() {
        /*making filePath = null so that prev file name is not used for upload*/
        filePath = null;
        captureBtn.setSelected(false);
        final CaptureConfiguration config = createCaptureConfiguration();
        Intent intent = new Intent(HomeActivity.this, VideoCaptureActivity.class);
        intent.putExtra(VideoCaptureActivity.EXTRA_CAPTURE_CONFIGURATION, config);
        intent.putExtra(EXTRA_OUTPUT_FILENAME, filePath);
        startActivity(intent);
//        finish();
    }

    private void updateStatusAndThumbnail() {
        if (statusMessage == null) {
            statusMessage = getString(R.string.status_nocapture);
        }
        statusTv.setText(statusMessage);
//        progressLayout.setVisibility(View.GONE);
    }

    private CaptureConfiguration createCaptureConfiguration() {
        final PredefinedCaptureConfigurations.CaptureResolution resolution = getResolution(2);
        final PredefinedCaptureConfigurations.CaptureQuality quality = getQuality(2);   /*low bitrate*/

        CaptureConfiguration.Builder builder = new CaptureConfiguration.Builder(resolution, quality);

        try {
            int maxDuration = Utils.getUtils().getLastDuration(this); /*2m 30s*/
            builder.maxDuration(maxDuration);
        } catch (final Exception e) {
            //NOP
            e.printStackTrace();
        }
        builder.showRecordingTime();
        return builder.build();
    }

    private PredefinedCaptureConfigurations.CaptureQuality getQuality(int position) {
        final PredefinedCaptureConfigurations.CaptureQuality[] quality = new PredefinedCaptureConfigurations.CaptureQuality[]{PredefinedCaptureConfigurations.CaptureQuality.HIGH, PredefinedCaptureConfigurations.CaptureQuality.MEDIUM,
                PredefinedCaptureConfigurations.CaptureQuality.LOW};
        return quality[position];
    }

    private PredefinedCaptureConfigurations.CaptureResolution getResolution(int position) {
        final PredefinedCaptureConfigurations.CaptureResolution[] resolution = new PredefinedCaptureConfigurations.CaptureResolution[]{PredefinedCaptureConfigurations.CaptureResolution.RES_1080P,
                PredefinedCaptureConfigurations.CaptureResolution.RES_720P, PredefinedCaptureConfigurations.CaptureResolution.RES_480P};
        return resolution[position];
    }

    private void startUpload(String filePath) {
        Intent intent;
        if (!Utils.getUtils().isS3Upload(this)) {
            int uniqueID = getIntent().getIntExtra(Constants.NOTIFIC_ID, 0);
            intent = new Intent(HomeActivity.this, UploadService.class);
            intent.putExtra(Constants.FILE_PATH, filePath);
            intent.putExtra(Constants.NOTIFIC_ID, uniqueID);
        } else {
            intent = new Intent(HomeActivity.this, S3UploadService.class);
            intent.putExtra(Constants.FILE_PATH, filePath);
        }
        startService(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                if (!this.isFinishing())
                    startActivity(new Intent(HomeActivity.this, SettingsActivity.class));
//                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(MESSAGE_PROGRESS)) {

//                if (progressLayout.getVisibility() == View.GONE)
//                    progressLayout.setVisibility(View.VISIBLE);
                Upload upload = intent.getParcelableExtra(Constants.UPLOAD);
                String uploadMsg = intent.getStringExtra(Constants.UPLOAD_MSG);

                String textMsg = "";
                String fileName = new File(upload.getFilePath()).getName();
                switch (NotificationUtils.NotificationType.valueOf(uploadMsg)) {

                    case UPLOADING:
                        textMsg = "Uploading video ...";
                        break;

                    case UPLOAD_WAITING_FOR_NETWORK:
                        textMsg = fileName + " upload waiting for network";
                        break;

                    case UPLOAD_FAILED:
                    case UPLOAD_FAILED_NETWORK:
                        textMsg = "Bad network, " + fileName + " uploading to server failed";
                        break;

                    case UPLOAD_CANCELLED:
                        textMsg = fileName + " uploading to server failed";
                        break;

                    case UPLOAD_FILE_NOT_FOUND:
                        textMsg = fileName + " video file not found";
                        break;

                    case UPLOAD_COMPLETED:
                        textMsg = fileName + " upload done";
                        break;

                    case UPLOAD_STARTED:
                        textMsg = fileName + " uploading started..";
                        break;
                }

                /*check if any layout is already added*/
                if (isLayoutAlreadyAdded(upload)) {
                    updateProgressLayout(upload, textMsg);
                } else {
                    addProgressLayout(upload, textMsg);
                }
            }
        }
    };

    private void addProgressLayout(Upload upload, String textMsg) {
        LinearLayout verticalLinearLayout = new LinearLayout(this);
        verticalLinearLayout.setOrientation(LinearLayout.VERTICAL);
        verticalLinearLayout.setLayoutParams(progressLayoutParams);

        ProgressBar uploadProgressBar = new ProgressBar(this, null, android.R.attr.progressBarStyleHorizontal);
        uploadProgressBar.setLayoutParams(progressLayoutParams);
        uploadProgressBar.setPadding(8, 8, 8, 8);
        uploadProgressBar.setProgress(upload.getProgress());

        TextView uploadProgressTextView = new TextView(this);
        uploadProgressTextView.setLayoutParams(progressLayoutParams);
        uploadProgressTextView.setPadding(8, 8, 8, 8);

        if (upload.getProgress() == 0
                /*&& upload.getTransferObserver()!=null
                && !upload.getTransferObserver().getState().equals(TransferState.IN_PROGRESS)*/) {
            uploadProgressTextView.setText(textMsg);
        } else if (upload.getProgress() == 100) {
            uploadProgressTextView.setText(textMsg);
        } else {
            String currentSize = Formatter.formatFileSize(HomeActivity.this, upload.getCurrentFileSize());
            String totalSize = Formatter.formatFileSize(HomeActivity.this, upload.getTotalFileSize());
            uploadProgressTextView.setText(textMsg + "....\t\t" + currentSize + "/" + totalSize + " done.");
        }

        verticalLinearLayout.setTag(upload.getNotificationId());
        verticalLinearLayout.addView(uploadProgressBar);
        verticalLinearLayout.addView(uploadProgressTextView);

        /*add to main LinearLayout*/
        uploadScrollLayout.addView(verticalLinearLayout);
    }

    private void registerReceiver() {
        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MESSAGE_PROGRESS);
        bManager.registerReceiver(broadcastReceiver, intentFilter);
    }

    private void unregisterReceiver() throws Exception {
        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(this);
        bManager.unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bt.disconnect();
            bt.stopService();
            unregisterReceiver();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isLayoutAlreadyAdded(Upload upload) {
        for (int k = 0; k < uploadScrollLayout.getChildCount(); k++) {
            LinearLayout progressLayout = (LinearLayout) uploadScrollLayout.getChildAt(k);
            int layoutSetNotificId = (int) progressLayout.getTag();
            if (upload.getNotificationId() == layoutSetNotificId)
                return true;
        }
        return false;
    }

    private void updateProgressLayout(Upload upload, String textMsg) {
        for (int k = 0; k < uploadScrollLayout.getChildCount(); k++) {
            LinearLayout progressLayout = uploadScrollLayout.findViewWithTag(upload.getNotificationId());
            ProgressBar progressBar = (ProgressBar) progressLayout.getChildAt(0);
            TextView progressText = (TextView) progressLayout.getChildAt(1);

            progressBar.setProgress(upload.getProgress());
            if (upload.getProgress() == 0) {
                progressText.setText(textMsg);
            } else if (upload.getProgress() == 100) {
                progressText.setText(textMsg);
            } else {
                String currentSize = Formatter.formatFileSize(HomeActivity.this, upload.getCurrentFileSize());
                String totalSize = Formatter.formatFileSize(HomeActivity.this, upload.getTotalFileSize());
                progressText.setText(textMsg + "....\t\t" + currentSize + "/" + totalSize + " done.");
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (mBackPressedInterval + TIME_INTERVAL > System.currentTimeMillis()) {
            if (backToast != null) {
                backToast.cancel();
            }
            super.onBackPressed();
            return;
        } else {
            if (backToast != null)
                backToast.show();
        }
        mBackPressedInterval = System.currentTimeMillis();
    }

    private void getMotherStatus() {

        final Utils utils = Utils.getUtils();

        if (!utils.isNetworkAvailable(this)) {
            utils.showEnableInternetDialog(this);
            return;
        }
//        utils.showLoader(SettingsActivity.this, null, "Deactivating user...");
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put(Constants.HEADER_API_KEY, utils.getUserApiKey(this));
        headerMap.put(Constants.HEADER_UID, utils.getUserUniqueId(this));

        RetrofitServiceGenerator.ApiService apiService = RetrofitServiceGenerator.createService(RetrofitServiceGenerator.ApiService.class);
        Call<GetStatusResponse> call = apiService.getUserStatus(headerMap);

        call.enqueue(new Callback<GetStatusResponse>() {
            @Override
            public void onResponse(Call<GetStatusResponse> call, Response<GetStatusResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null && response.body().getStatus() != null) {
                        String status = response.body().getStatus();
                        if (status.equalsIgnoreCase("success")) {
                            String records = response.body().getRecords();
                            Log.d("getMotherStatus", "records : " + records);
                            if (records.equalsIgnoreCase("Inactive") || records.equalsIgnoreCase("Deleted")) {
                                Utils.getUtils().clearCredentials(HomeActivity.this);
                                startActivity(new Intent(HomeActivity.this, RegisterActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                                finish();
                            }
                        }
                    }
                } else {
                    Log.d("getMotherStatus", "error = " + response.message());
                    if (response.message().equalsIgnoreCase("Unauthorized")) {
                        Utils.getUtils().clearCredentials(HomeActivity.this);
                        startActivity(new Intent(HomeActivity.this, RegisterActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                        finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<GetStatusResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void initiBlueToothService() {
        if (!bt.isBluetoothEnabled()) {
            bt.enable();
        } else {
            if (!bt.isServiceAvailable()) {
                bt.setupService();
                bt.startService(false);
            }
        }
    }

    private void setUpBTConnectionLisenter() {
        bt.setBluetoothConnectionListener(new BluetoothSPP.BluetoothConnectionListener() {
            public void onDeviceConnected(String name, String address) {
                Log.e("!!", "onDeviceConnected");
                showToast("Connected to " + address);
                dissMisDialog();
                if (captureBtn.isSelected()) {
                    startVideoCaptureActivity();
                }
            }

            public void onDeviceDisconnected() {
                Log.e("!!", "onDeviceDisconnected");
                showToast("Disconnected");
                dissMisDialog();
                if (captureBtn.isSelected()) {
                    startVideoCaptureActivity();
                }
            }

            public void onDeviceConnectionFailed() {
                Log.e("!!", "onDeviceConnectionFailed");
                showToast("ConnectionFailed");
                dissMisDialog();
                if (captureBtn.isSelected()) {
                    startVideoCaptureActivity();
                }
            }
        });
    }

    private void setUPDataReceiver() {
        bt.setOnDataReceivedListener(new BluetoothSPP.OnDataReceivedListener() {
            @Override
            public void onDataReceived(byte[] data, String message) {
                Log.e("!!received data", message);
                Intent intent = new Intent(Constants.ACTION_RECEIVE_DATA);
                intent.putExtra("data", data);
                intent.putExtra("stringvalue", message);
                sendBroadcast(intent);
            }
        });
    }

    private void showDialog(String message) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
        }

        progressDialog.setMessage(message);
        progressDialog.show();
    }

    private void dissMisDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    private void showToast(String message) {
        Toast.makeText(HomeActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    private void checkForPermission() {
        if (ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(HomeActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
            return;
        } else {
            checkFOrBlueAndEnable();
            // Write you code here if permission already given.
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LOCATION_REQUEST_CODE) {
            if (ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                checkFOrBlueAndEnable();
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(HomeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                    Toast.makeText(HomeActivity.this, "GPS permission allows us to access location data. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
                } else {
                    finish();
                }
            }
        }
    }

    private void checkFOrBlueAndEnable() {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter.isEnabled()) {
            connectToDevice();
        } else {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ENABLE_BT) {
            checkFOrBlueAndEnable();
        }
    }

    private void connectToDevice() {
        String deviceAddress = sharedPreferences.getString(Constants.BLUETOOTH_DEVICE_ADDRESS, "");
        showDialog("Connecting to device " + deviceAddress);
        if (!bt.isBluetoothEnabled()) {
            bt.enable();
        }
        if (!bt.isServiceAvailable()) {
            bt.setupService();
            bt.startService(false);
        }
        bt.connect(deviceAddress);
    }
}
