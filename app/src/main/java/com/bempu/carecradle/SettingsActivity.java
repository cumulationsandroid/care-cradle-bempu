package com.bempu.carecradle;

import android.Manifest;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Camera;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bempu.carecradle.configuration.CaptureConfiguration;
import com.bempu.carecradle.configuration.PredefinedCaptureConfigurations;
import com.bempu.carecradle.models.DeactivateResponse;
import com.bempu.carecradle.models.VideoCountResponse;
import com.bempu.carecradle.network.RetrofitServiceGenerator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static app.akexorcist.bluetotohspp.library.BluetoothState.REQUEST_ENABLE_BT;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private final String[] RESOLUTION_NAMES = new String[]{"1080p", "720p", "480p"};
    private final String[] QUALITY_NAMES = new String[]{"high", "medium", "low"};

    //    private EditText maxDurationEt;
    private Button saveDurationBtn, saveFocusModeBtn;
    private Button deactivateButton;
    private TextView videosCount;
    private boolean isFetchApiCalled;
    private TextInputLayout mmInputLayout, ssInputLayout;
    private TextInputEditText mmEditText, ssEditText;
    private RadioButton fmVidRb, fmPicRb, fmAutoRb, noFmRb;
    public static String selectedFocusMode;
    private Switch connectDeviceSwitch;
    private ImageView editDeviceAddress;
    private SharedPreferences sharedPreferences;
    private LinearLayout deviceAddressLayout;
    private TextView deviceAddressTv;
    private int LOCATION_REQUEST_CODE = 2010;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        mmInputLayout = findViewById(R.id.duration_mm_input_layout);
        mmEditText = findViewById(R.id.duration_mm_InputEt);
        ssInputLayout = findViewById(R.id.duration_ss_input_layout);
        ssEditText = findViewById(R.id.duration_ss_InputEt);
        saveDurationBtn = (Button) findViewById(R.id.save_duration_btn);
        fmVidRb = findViewById(R.id.focusModeVideoRb);
        fmPicRb = findViewById(R.id.focusModePicRb);
        fmAutoRb = findViewById(R.id.focusModeAutoRb);
        noFmRb = findViewById(R.id.noFocusModeRb);
        saveFocusModeBtn = findViewById(R.id.save_focus_mode_btn);

        connectDeviceSwitch = findViewById(R.id.connect_device_sw);
        editDeviceAddress = findViewById(R.id.edit_bluetooth_address);
        deviceAddressLayout = findViewById(R.id.device_address_layout);
        deviceAddressTv = findViewById(R.id.device_address_tv);

        deactivateButton = (Button) findViewById(R.id.deactivate_btn);
        videosCount = (TextView) findViewById(R.id.videos_count);

        saveDurationBtn.setOnClickListener(this);
        saveFocusModeBtn.setOnClickListener(this);
        deactivateButton.setOnClickListener(this);
        editDeviceAddress.setOnClickListener(this);

        initDuration();
        initFocusModes();
        fetchVideosCount();
        setUpConnectDeviceUI();
        connectDeviceSwitch.setOnCheckedChangeListener(this);

        mmEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() >= 1) {
                    int mm = Integer.valueOf(editable.toString());
                    if (mm > 2) {
                        Toast.makeText(SettingsActivity.this, "Minutes cannot be more than 2", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        ssEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 1) {
                    int ss = Integer.valueOf(editable.toString());
                    if (mmEditText.getText().length() > 1 && Integer.valueOf(mmEditText.getText().toString()) == 2 && ss > 30) {
                        Toast.makeText(SettingsActivity.this, "Max recording time allowed is 2 Min 30 Sec", Toast.LENGTH_SHORT).show();
                    }
                    if (ss > 59) {
                        Toast.makeText(SettingsActivity.this, "Seconds cannot be more than 59", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void setUpConnectDeviceUI() {
        boolean connectionState = sharedPreferences.getBoolean(Constants.DEVICE_CONNECTION_STATE, false);
        connectDeviceSwitch.setChecked(connectionState);
        String deviceAddress = sharedPreferences.getString(Constants.BLUETOOTH_DEVICE_ADDRESS, "");

        if (connectionState) {
            if (!deviceAddress.isEmpty()) {
                deviceAddressLayout.setVisibility(View.VISIBLE);
                deviceAddressTv.setText("Device Address : " + deviceAddress);
            }
        } else {
            deviceAddressLayout.setVisibility(View.GONE);
        }
    }

    private void initFocusModes() {
        List<String> fModes = Utils.getUtils().getSupportedFocusModes();
        if (fModes != null) {
            if (fModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO)) {
                fmVidRb.setVisibility(View.VISIBLE);
            }

            if (fModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
                fmPicRb.setVisibility(View.VISIBLE);
            }

            if (fModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
                fmAutoRb.setVisibility(View.VISIBLE);
            }
        }

        String mode = Utils.getUtils().getFocusMode(this);
        switch (mode) {
            case Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO:
                fmVidRb.setChecked(true);
                break;

            case Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE:
                fmPicRb.setChecked(true);
                break;

            case Camera.Parameters.FOCUS_MODE_AUTO:
                fmAutoRb.setChecked(true);
                break;

            default:
                noFmRb.setChecked(true);
        }
    }

    @Override
    public void onClick(View view) {
        Utils.getUtils().closeKeyboard(this, getCurrentFocus());
        switch (view.getId()) {
            case R.id.save_duration_btn:
                if (getTotalDurationInSeconds() == 0) {
                    Toast.makeText(this, "Enter valid duration in MM : ss", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (mmEditText.getText().length() >= 1) {
                    int minutes = Integer.valueOf(mmEditText.getText().toString());
                    if (minutes > 2) {
                        mmEditText.requestFocus();
                        Toast.makeText(SettingsActivity.this, "Minutes cannot be more than 2", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                if (ssEditText.getText().length() > 1) {
                    int sec = Integer.valueOf(ssEditText.getText().toString());
                    if (sec > 59) {
                        Toast.makeText(SettingsActivity.this, "Seconds cannot be more than 59", Toast.LENGTH_SHORT).show();
                        ssEditText.requestFocus();
                        return;
                    }
                }

                if (Integer.valueOf(mmEditText.getText().toString()) == 2 && Integer.valueOf(ssEditText.getText().toString()) > 30) {
                    Toast.makeText(SettingsActivity.this, "Max recording time allowed is 2 Min 30 Sec", Toast.LENGTH_SHORT).show();
                    ssEditText.requestFocus();
                    return;
                }

                showSettingsPwdDialog(Constants.USER_VIDEO_DURATION);
                break;

            case R.id.save_focus_mode_btn:
                if (fmVidRb.isChecked())
                    selectedFocusMode = Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO;
                if (fmPicRb.isChecked())
                    selectedFocusMode = Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE;
                if (fmAutoRb.isChecked())
                    selectedFocusMode = Camera.Parameters.FOCUS_MODE_AUTO;
                if (noFmRb.isChecked())
                    selectedFocusMode = Constants.NO_FOCUS_MODE;

                showSettingsPwdDialog(Constants.USER_CAM_FOCUS_MODE);
                break;

            case R.id.deactivate_btn:
//                showConfirmationDialog();
                showSettingsPwdDialog(Constants.USER_DEACTIVATE);
                break;

            case R.id.edit_bluetooth_address:
                editDeviceAddress.setSelected(true);
                checkForPermission();
                break;
        }
    }

    private int getTotalDurationInSeconds() {
        if (mmEditText.getText().toString().isEmpty() || ssEditText.getText().toString().isEmpty()) {
            return 0;
        }
        int mmInSec = Integer.valueOf(mmEditText.getText().toString()) * 60;
        return mmInSec + Integer.valueOf(ssEditText.getText().toString());
    }

    private void deactivateUser() {
        final Utils utils = Utils.getUtils();

        if (!utils.isNetworkAvailable(SettingsActivity.this)) {
            utils.showEnableInternetDialog(SettingsActivity.this);
            return;
        }
        utils.showLoader(SettingsActivity.this, null, "Deactivating user...");
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put(Constants.HEADER_API_KEY, utils.getUserApiKey(SettingsActivity.this));
        headerMap.put(Constants.HEADER_UID, utils.getUserUniqueId(SettingsActivity.this));

        RetrofitServiceGenerator.ApiService apiService = RetrofitServiceGenerator.createService(RetrofitServiceGenerator.ApiService.class);
        Call<DeactivateResponse> call = apiService.deactivateUser(headerMap);

        call.enqueue(new Callback<DeactivateResponse>() {
            @Override
            public void onResponse(Call<DeactivateResponse> call, Response<DeactivateResponse> response) {
                utils.closeLoader();
                if (response.isSuccessful()) {
                    String status = response.body().getStatus();
                    if (status == null)
                        return;
                    if (status.equalsIgnoreCase("success")) {
                        Toast.makeText(SettingsActivity.this, "User Deactivated", Toast.LENGTH_SHORT).show();
                        deactivateButton.setEnabled(true);
                        deactivateButton.setClickable(true);
                        Utils.getUtils().clearCredentials(SettingsActivity.this);
                        startActivity(new Intent(SettingsActivity.this, RegisterActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                        finish();
                    } else if (status.equalsIgnoreCase("error")) {
                        String desc = response.body().getDescription();
                        if (desc != null) {
                            if (desc.equalsIgnoreCase("invalid uniqueid")) {
                                Toast.makeText(SettingsActivity.this, "Mother ID is already Deactivated", Toast.LENGTH_SHORT).show();
                                Utils.getUtils().clearCredentials(SettingsActivity.this);
                                startActivity(new Intent(SettingsActivity.this, RegisterActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                                finish();
                                return;
                            } else
                                Toast.makeText(SettingsActivity.this, "Error : " + desc, Toast.LENGTH_SHORT).show();
                        }

                    }
                } else {
                    Toast.makeText(SettingsActivity.this, "Error : " + response.message(), Toast.LENGTH_SHORT).show();
                    deactivateButton.setEnabled(true);
                    deactivateButton.setClickable(true);
                }
            }

            @Override
            public void onFailure(Call<DeactivateResponse> call, Throwable t) {
                utils.closeLoader();
                deactivateButton.setEnabled(true);
                deactivateButton.setClickable(true);
                Toast.makeText(SettingsActivity.this, "Error : " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    /*private void showConfirmationDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SettingsActivity.this);
        // set title
        alertDialogBuilder.setTitle(getString(R.string.title));

        // set dialog message
        alertDialogBuilder
                .setTitle(getString(R.string.deactivate_dialog_title))
                .setMessage(getString(R.string.deactivate_confirmation)+" "+Utils.getUtils().getUserUniqueId(SettingsActivity.this)+" ?")
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        deactivateUser();
                    }
                })
                .setNegativeButton(getString(R.string.Cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create().show();
    }*/

    private void initDuration() {
        Utils utils = Utils.getUtils();
        mmEditText.setText(utils.inMinutes(utils.getLastDuration(this)));
        ssEditText.setText(utils.inSeconds(utils.getLastDuration(this)));
    }

    private void fetchVideosCount() {

        if (isFetchApiCalled)
            return;

        final Utils utils = Utils.getUtils();

        if (!utils.isNetworkAvailable(SettingsActivity.this)) {
//            utils.showEnableInternetDialog(SettingsActivity.this);
            videosCount.setText("Videos Uploaded : Error, check network connection");
            return;
        }
//        utils.showLoader(SettingsActivity.this, null, "Deactivating user...");
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put(Constants.HEADER_API_KEY, utils.getUserApiKey(SettingsActivity.this));
        headerMap.put(Constants.HEADER_UID, utils.getUserUniqueId(SettingsActivity.this));

        RetrofitServiceGenerator.ApiService apiService = RetrofitServiceGenerator.createService(RetrofitServiceGenerator.ApiService.class);
        Call<VideoCountResponse> call = apiService.getVidsCount(headerMap);

        call.enqueue(new Callback<VideoCountResponse>() {
            @Override
            public void onResponse(Call<VideoCountResponse> call, Response<VideoCountResponse> response) {
                isFetchApiCalled = true;
                if (response.isSuccessful()) {
                    if (response.body() == null) return;
                    String msg = response.body().getStatus();
                    if (msg != null && !msg.isEmpty() && msg.equalsIgnoreCase("success")) {
                        videosCount.setText("Videos Uploaded : " + response.body().getCount());
                    }
                } else {
                    Log.d("fetchVideosCount", "error = " + response.message());
                    videosCount.setText("Videos Uploaded : " + response.message());
                    if (response.message().equalsIgnoreCase("Unauthorized")) {
                        Utils.getUtils().clearCredentials(SettingsActivity.this);
                        startActivity(new Intent(SettingsActivity.this, RegisterActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                        finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<VideoCountResponse> call, Throwable t) {
                t.printStackTrace();
                videosCount.setText("Videos Uploaded : " + t.getMessage());
            }
        });
    }

    private void showSettingsPwdDialog(final String type) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SettingsActivity.this);
        View customDialogView = View.inflate(SettingsActivity.this, R.layout.layout_custom_pwd_dialog, null);
        alertDialogBuilder.setView(customDialogView);

        final TextInputLayout pwdInputLayout = customDialogView.findViewById(R.id.pwd_input_layout);
        final AppCompatEditText pwdEditText = customDialogView.findViewById(R.id.pwd_edittext);

        // set dialog message
        final AlertDialog customDialog = alertDialogBuilder
                .setTitle(getString(R.string.pwd_title))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), null) //Set to null. We override the onclick
                .setNegativeButton(getString(R.string.close), null)
                .create();

        customDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialog) {

                Button positiveBtn = customDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                positiveBtn.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        // TODO Do something
                        if (TextUtils.isEmpty(pwdEditText.getText().toString())) {
                            pwdInputLayout.setErrorEnabled(true);
                            pwdInputLayout.setError("Enter Password !!");
//                            return;
                        } else if (!pwdEditText.getText().toString().equals(BuildConfig.SETTINGS_PWD)) {
                            pwdInputLayout.setErrorEnabled(true);
                            pwdInputLayout.setError("Wrong Password");
//                            return;
                        } else {

                            pwdInputLayout.setErrorEnabled(false);
                            pwdInputLayout.setError(null);
                            Utils.getUtils().closeKeyboard(SettingsActivity.this, getCurrentFocus());
                            customDialog.cancel();
                            if (type.equalsIgnoreCase(Constants.USER_VIDEO_DURATION)) {
                                Utils.getUtils().saveDuration(SettingsActivity.this, getTotalDurationInSeconds());
                                finish();
                            } else if (type.equalsIgnoreCase(Constants.USER_CAM_FOCUS_MODE)) {
                                Utils.getUtils().saveFocusMode(SettingsActivity.this, selectedFocusMode);
                                finish();
                            } else if (type.equalsIgnoreCase(Constants.USER_DEACTIVATE)) {
                                deactivateUser();
                            }
                        }

                    }
                });

                Button negativeBtn = customDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
                negativeBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Utils.getUtils().closeKeyboard(SettingsActivity.this, getCurrentFocus());
                        customDialog.cancel();
                    }
                });
            }
        });

        customDialog.show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        selectedFocusMode = Utils.getUtils().getFocusMode(this);
    }

    private void checkForPermission() {
        if (ActivityCompat.checkSelfPermission(SettingsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(SettingsActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(SettingsActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
            return;
        } else {
            checkFOrBlueAndEnable();
            // Write you code here if permission already given.
        }
    }

    private void checkFOrBlueAndEnable() {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter.isEnabled()) {
            if (!sharedPreferences.getString(Constants.BLUETOOTH_DEVICE_ADDRESS, "").isEmpty()) {
                if (editDeviceAddress.isSelected()) {
                    showDeviceListDialog();
                }
                setUpConnectDeviceUI();
            } else {
                showDeviceListDialog();
            }
        } else {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
    }

    private void showDeviceListDialog() {
        editDeviceAddress.setSelected(false);
        Set<BluetoothDevice> pairedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices();
        ArrayList<String> devicelist = new ArrayList();
        final HashMap<String, BluetoothDevice> map = new HashMap<>();

        for (BluetoothDevice btD : pairedDevices) {
            String key = btD.getName() + "  " + btD.getAddress();
            map.put(key, btD);
            devicelist.add(key);
        }
        devicelist.add(0, "If Devices are not paired go to setting and paired the device");
        final String[] array = devicelist.toArray(new String[devicelist.size()]);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Paired Devices")
                .setItems(array, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        BluetoothDevice bluetoothDevice = map.get(array[which]);
                        sharedPreferences.edit().putString(Constants.BLUETOOTH_DEVICE_ADDRESS, bluetoothDevice.getAddress()).apply();
                        Log.e("!!", "selected item " + bluetoothDevice.getName() + "," + bluetoothDevice.getAddress());
                        setUpConnectDeviceUI();
                    }
                });

        builder.setPositiveButton("Refresh", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                showDeviceListDialog();
            }
        });

        AlertDialog alertDialog = builder.create();

        ListView listView = alertDialog.getListView();
        listView.setDivider(new ColorDrawable(Color.BLUE)); // set color
        listView.setDividerHeight(2);

        alertDialog.show();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            checkForPermission();
        }
        sharedPreferences.edit().putBoolean(Constants.DEVICE_CONNECTION_STATE, isChecked).apply();
        setUpConnectDeviceUI();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LOCATION_REQUEST_CODE) {
            if (ActivityCompat.checkSelfPermission(SettingsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(SettingsActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                checkFOrBlueAndEnable();
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(SettingsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                    Toast.makeText(SettingsActivity.this, "GPS permission allows us to access location data. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
                } else {
                    finish();
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_ENABLE_BT) {
            checkFOrBlueAndEnable();
        }
    }
}
