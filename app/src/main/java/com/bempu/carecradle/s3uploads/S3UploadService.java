package com.bempu.carecradle.s3uploads;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.text.format.Formatter;
import android.util.Log;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferType;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.bempu.carecradle.Constants;
import com.bempu.carecradle.NotificationUtils;
import com.bempu.carecradle.R;
import com.bempu.carecradle.RegisterActivity;
import com.bempu.carecradle.Utils;
import com.bempu.carecradle.models.FailedUploadEvent;
import com.bempu.carecradle.models.Upload;
import com.bempu.carecradle.models.UploadResponse;
import com.bempu.carecradle.network.RetrofitServiceGenerator;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.SSLException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Amit T on 31-10-2017.
 */

public class S3UploadService extends Service {
    private NotificationUtils notificationUtils;
    // The TransferUtility is the primary class for managing transfer to S3
    private TransferUtility transferUtility;
    private HashMap<Integer,Object> uploadsHashMap;
    private TransferListener transferListener;
    private HashMap<String,Object> s3EventsHashMap;

    @Override
    public void onCreate() {
        super.onCreate();

        notificationUtils = NotificationUtils.getNotificationUtils();
        notificationUtils.build(this);
        // Initializes TransferUtility, always do this before using it.
        transferUtility = S3Util.getTransferUtility(this);
        uploadsHashMap = new HashMap<>();
        s3EventsHashMap = new HashMap<>();
        transferListener = new UploadListener();
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        String filePath;
        if (intent != null) {
        /*make service run every 5 mins*/
            setRecurringAlarm(this);
            filePath = intent.getStringExtra(Constants.FILE_PATH);

            if (filePath == null) {
                Log.d("onStartCommand", "empty intent");
                s3uploadFailedVideos();
            } else {
                initS3upload(filePath);
            }
        }
        return START_STICKY;
    }

    private void s3uploadFailedVideos() {
        List<TransferObserver> pendingTransferObserverList = transferUtility.getTransfersWithType(TransferType.UPLOAD);
        if (pendingTransferObserverList==null || pendingTransferObserverList.size() == 0){
            Log.d("s3uploadFailedVideos","total vids in upload zero");
            return;
        }

        Log.d("s3uploadFailedVideos","total vids in upload : "+pendingTransferObserverList.size());
        for (TransferObserver observer : pendingTransferObserverList){
            // Sets listeners to in progress transfers
            Log.d("s3uploadFailedVideos","id "+observer.getId()+" state "+observer.getState());
            if (TransferState.WAITING.equals(observer.getState())
                    || TransferState.WAITING_FOR_NETWORK.equals(observer.getState())
                    || TransferState.IN_PROGRESS.equals(observer.getState())) {
                resumeS3upload(observer);
            } else if (observer.getState().equals(TransferState.FAILED)
                    || observer.getState().equals(TransferState.UNKNOWN)
                    || observer.getState().equals(TransferState.CANCELED)){
                observer.cleanTransferListener();
                transferUtility.deleteTransferRecord(observer.getId());
                if (uploadsHashMap.containsKey(observer.getId()))
                    uploadsHashMap.remove(observer.getId());
                String failedFilePath = observer.getAbsoluteFilePath();
                initS3upload(failedFilePath);
            }
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void sendNotification(Upload upload) {
        if (upload.getTransferObserver().getState().equals(TransferState.IN_PROGRESS)) {
            sendIntent(upload, NotificationUtils.NotificationType.UPLOADING.toString());
            notificationUtils.showNotification(upload, NotificationUtils.NotificationType.UPLOADING);
        }
    }

    private void sendIntent(Upload upload,String uploadMsg){
        Intent intent = new Intent(Constants.MESSAGE_PROGRESS);
        intent.putExtra(Constants.UPLOAD,upload);
        intent.putExtra(Constants.UPLOAD_MSG,uploadMsg);
        LocalBroadcastManager.getInstance(S3UploadService.this).sendBroadcast(intent);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
//        notificationManager.cancel(rootIntent.getIntExtra(Constants.NOTIFIC_ID,0));
    }

    private void setRecurringAlarm(Context context) {
        Intent s3uploadService = new Intent(context, S3UploadService.class);
        s3uploadService.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getService(context, 0, s3uploadService, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        // should be AlarmManager.INTERVAL_DAY (but changed to 15min for testing)
        long triggerAtMillis = System.currentTimeMillis()+ 300000; /*5 mins*/
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, triggerAtMillis,
                /*AlarmManager.INTERVAL_FIFTEEN_MINUTES*/300000, pendingIntent);
        Log.d("S3UploadService", "Set alarmManager.setRepeating to: " + System.currentTimeMillis());
    }

    private void initS3upload(String filePath){
        Upload upload = new Upload();
        upload.setFilePath(filePath);
        upload.setNotificationId(0);

        /*For sending event to fabrics in case of failed uploads*/
        FailedUploadEvent failedUploadEvent = new FailedUploadEvent();
        failedUploadEvent.setuId(Utils.getUtils().getUserUniqueId(this));
        failedUploadEvent.setModel(Utils.getUtils().getDeviceName());
        failedUploadEvent.setNetworkType(S3EventUtil.getS3EventUtil().getNetworkClass(this));
        failedUploadEvent.setName(new File(upload.getFilePath()).getName());
        failedUploadEvent.setCarrier(S3EventUtil.getS3EventUtil().getPrimarySimOperatorName(this));
        long bytesTotal = new File(upload.getFilePath()).length();
        failedUploadEvent.setSize(Formatter.formatFileSize(S3UploadService.this,bytesTotal));

        if (filePath == null) {
            Toast.makeText(this, getString(R.string.filepath_error), Toast.LENGTH_SHORT).show();
            return;
        }

        File videoFile = new File(filePath);
        if (!videoFile.exists()){
            Log.d("initS3upload","video file not found = "+videoFile.getAbsolutePath());
            sendIntent(upload, NotificationUtils.NotificationType.UPLOAD_FILE_NOT_FOUND.toString());
            notificationUtils.showNotification(upload, NotificationUtils.NotificationType.UPLOAD_FILE_NOT_FOUND);
            return;
        }

        TransferObserver observer = transferUtility.upload(S3Constants.BUCKET_NAME, videoFile.getName(), videoFile);

        upload.setNotificationId(observer.getId());
        upload.setTransferObserver(observer);
        uploadsHashMap.put(upload.getNotificationId(), upload);
        observer.setTransferListener(transferListener);

        failedUploadEvent.setUploadTime(S3EventUtil.getS3EventUtil().getCurrentTime());
        s3EventsHashMap.put(upload.getFilePath(), failedUploadEvent);

        Log.d("upload start",videoFile.getName()+" "+System.currentTimeMillis());
    }

    /*
     * A TransferListener class that can listen to a upload task and be notified
     * when the status changes.
     */
    private class UploadListener implements TransferListener {
        private String TAG = UploadListener.class.getSimpleName();

        // Simply updates the UI list when notified.
        @Override
        public void onError(int id, Exception e) {
            Log.e(TAG, "Error during upload: " + id, e.getCause());
//            Toast.makeText(S3UploadService.this, "Upload error : "+e.getCause().getMessage(), Toast.LENGTH_SHORT).show();

            Upload upload = (Upload) uploadsHashMap.get(id);
            if (upload!=null) {
                upload.setProgress(0);
                String msg = null;
                String s3eventMsg = null;
                if (e instanceof FileNotFoundException || e.getCause() instanceof FileNotFoundException) {
                    /*remove transferListener so that FAILED doesn't get called and show as uploading to server failed*/
                    if (upload.getTransferObserver()!=null)
                        upload.getTransferObserver().cleanTransferListener();

                    transferUtility.deleteTransferRecord(upload.getTransferObserver().getId());
                    if (uploadsHashMap.containsKey(id))
                        uploadsHashMap.remove(id);
                    notificationUtils.showNotification(upload, NotificationUtils.NotificationType.UPLOAD_FILE_NOT_FOUND);
                    msg = NotificationUtils.NotificationType.UPLOAD_FILE_NOT_FOUND.toString();
                    s3eventMsg = "file_not_found";
                }
                if (e instanceof InterruptedException || e.getCause() instanceof InterruptedException
                        || e instanceof SocketTimeoutException || e.getCause() instanceof SocketTimeoutException
                        || e instanceof UnknownHostException || e.getCause() instanceof UnknownHostException
                        || e instanceof SSLException || e.getCause() instanceof SSLException) {
                    notificationUtils.showNotification(upload, NotificationUtils.NotificationType.UPLOAD_FAILED_NETWORK);
                    msg = NotificationUtils.NotificationType.UPLOAD_FAILED_NETWORK.toString();
                    if (e.getMessage() == null){
                        if (e.getCause()!=null && e.getCause().getMessage()!=null){
                            s3eventMsg = e.getCause().getMessage();
                        }
                    } else {
                        s3eventMsg = e.getMessage();
                    }
//                    s3eventMsg = "network_failure";
                }
                if (msg!=null) {
                    sendIntent(upload, msg);

                    /*updating failure event to fabric*/
                    FailedUploadEvent failedUploadEvent = (FailedUploadEvent) s3EventsHashMap.get(upload.getFilePath());
                    if (failedUploadEvent !=null){
                        failedUploadEvent.setFailureTime(S3EventUtil.getS3EventUtil().getCurrentTime());
                        s3eventMsg = s3eventMsg+"_"+ failedUploadEvent.getFailureTime();
                        failedUploadEvent.setFailure(s3eventMsg);

                        logEventToFabrics(failedUploadEvent);
                    }
                }
                /*Update hashmap*/
                uploadsHashMap.put(id,upload);
            }
        }

        @Override
        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
            Log.d(TAG, String.format("onProgressChanged: %d, total: %d, current: %d",
                    id, bytesTotal, bytesCurrent));
            Upload upload = (Upload) uploadsHashMap.get(id);
            if (upload!=null) {
                Log.d("onProgressChanged","Uplaod Id = "+upload.getNotificationId()+", file = "+upload.getFilePath());
                int progress = (int) ((double) bytesCurrent * 100 / bytesTotal);
                if (progress>=0) {
                    /*to avoid showing upload failure in Home screen when progress is less then 1*/
                    upload.setProgress(progress);
                    upload.setTotalFileSize((int) bytesTotal);
                    upload.setCurrentFileSize((int) bytesCurrent);
                    sendNotification(upload);
                }
                /*Update hashmap*/
                uploadsHashMap.put(id,upload);

                FailedUploadEvent failedUploadEvent = (FailedUploadEvent) s3EventsHashMap.get(upload.getFilePath());
                if (failedUploadEvent !=null){
//                    s3Event.setSize(Formatter.formatFileSize(S3UploadService.this,bytesTotal));
                    failedUploadEvent.setBytesFailed(Formatter.formatFileSize(S3UploadService.this,bytesCurrent));
                    failedUploadEvent.setUploadPercent(String.valueOf(progress));
                }
            }

        }

        @Override
        public void onStateChanged(int id, TransferState newState) {
            Log.d(TAG, "onStateChanged: " + id + ", " + newState);
            Upload upload = (Upload) uploadsHashMap.get(id);
            if (upload == null)
                return;
            switch (newState){
                case WAITING:
                    upload.setProgress(0);
                    sendIntent(upload,NotificationUtils.NotificationType.UPLOAD_STARTED.toString());
                    notificationUtils.showNotification(upload, NotificationUtils.NotificationType.UPLOAD_STARTED);
                    break;

                case PAUSED:
                case WAITING_FOR_NETWORK:
                    upload.setProgress(0);
                    sendIntent(upload,NotificationUtils.NotificationType.UPLOAD_WAITING_FOR_NETWORK.toString());
                    notificationUtils.showNotification(upload, NotificationUtils.NotificationType.UPLOAD_WAITING_FOR_NETWORK);
                    break;

                case FAILED:
                case UNKNOWN:
                    Log.d("upload failed",new File(upload.getFilePath()).getName()+" "+System.currentTimeMillis());
                    upload.setProgress(0);
                    sendIntent(upload,NotificationUtils.NotificationType.UPLOAD_FAILED.toString());
                    upload.getTransferObserver().cleanTransferListener();
                    notificationUtils.showNotification(upload, NotificationUtils.NotificationType.UPLOAD_FAILED);
                    break;

                case CANCELED:
                    upload.setProgress(0);
                    sendIntent(upload,NotificationUtils.NotificationType.UPLOAD_CANCELLED.toString());
                    upload.getTransferObserver().cleanTransferListener();
                    notificationUtils.showNotification(upload, NotificationUtils.NotificationType.UPLOAD_CANCELLED);
                    break;

                case COMPLETED:
                    final Upload postUpload = upload;
                    Log.d("upload complete",new File(postUpload.getFilePath()).getName()+" "+System.currentTimeMillis());
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            postUpload.setProgress(100);
                            sendIntent(postUpload,NotificationUtils.NotificationType.UPLOAD_COMPLETED.toString());
                            postUpload.getTransferObserver().cleanTransferListener();
                            notificationUtils.showNotification(postUpload, NotificationUtils.NotificationType.UPLOAD_COMPLETED);
                            sendVideoUrlToCloud(postUpload);
                        }
                    },1000);
                    break;

                case IN_PROGRESS:
                    sendIntent(upload,NotificationUtils.NotificationType.UPLOADING.toString());
                    notificationUtils.showNotification(upload, NotificationUtils.NotificationType.UPLOADING);
                    break;
            }

            /*Update hashmap*/
            uploadsHashMap.put(id,upload);
        }
    }

    private void logEventToFabrics(FailedUploadEvent failedUploadEvent) {
        try {
            String attrValue = S3EventUtil.getS3EventUtil().getFabricAttributeValue(failedUploadEvent);
            /*Answers.getInstance().logCustom(new CustomEvent("Failed Uploads")
                    .putCustomAttribute("Upload Details", attrValue));*/
            Answers.getInstance().logCustom(new CustomEvent("Failed Uploads")
                    .putCustomAttribute("User ID", failedUploadEvent.getuId())
                    .putCustomAttribute("Device Model", failedUploadEvent.getModel())
                    .putCustomAttribute("Name", failedUploadEvent.getName())
                    .putCustomAttribute("File Size", failedUploadEvent.getSize())
                    .putCustomAttribute("Upload Time", failedUploadEvent.getUploadTime())
                    .putCustomAttribute("Network Type", failedUploadEvent.getNetworkType())
                    .putCustomAttribute("Carrier", failedUploadEvent.getCarrier())
                    .putCustomAttribute("Upload Percent", failedUploadEvent.getUploadPercent())
                    .putCustomAttribute("Failure Reason", failedUploadEvent.getFailure())
                    .putCustomAttribute("Failure Time", failedUploadEvent.getFailureTime())
                    .putCustomAttribute("Size Failed", failedUploadEvent.getBytesFailed())
            );
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void resumeS3upload(TransferObserver observer){
        Log.d("resumeS3upload","Id = "+observer.getId()+", file = "+observer.getAbsoluteFilePath());
        if (observer.getAbsoluteFilePath() == null) {
            Toast.makeText(this, getString(R.string.filepath_error), Toast.LENGTH_SHORT).show();
            Log.d("resumeS3upload",getString(R.string.filepath_error));
            observer.cleanTransferListener();
            transferUtility.deleteTransferRecord(observer.getId());
            return;
        }

        Upload upload = new Upload();
        upload.setFilePath(observer.getAbsoluteFilePath());
        upload.setNotificationId(observer.getId());

        if (!new File(observer.getAbsoluteFilePath()).exists()){
            if (uploadsHashMap.containsKey(observer.getId()))
                uploadsHashMap.remove(observer.getId());
            observer.cleanTransferListener();
            transferUtility.deleteTransferRecord(observer.getId());
            upload.setTransferObserver(observer);
            sendIntent(upload, NotificationUtils.NotificationType.UPLOAD_FILE_NOT_FOUND.toString());
            notificationUtils.showNotification(upload, NotificationUtils.NotificationType.UPLOAD_FILE_NOT_FOUND);


            if (s3EventsHashMap.containsKey(observer.getAbsoluteFilePath()))
                s3EventsHashMap.remove(observer.getAbsoluteFilePath());

            return;
        }

        /*resume video upload*/
        TransferObserver resumed = transferUtility.resume(observer.getId());

        /**
         * If resume returns null, it is likely because the transfer
         * is not in a resumable state (For instance it is already
         * running).
         */
        if (resumed == null) {
            Toast.makeText(S3UploadService.this, "Cannot resume upload.Only paused upload can be resumed.", Toast.LENGTH_SHORT).show();
            return;
        }

        upload.setTransferObserver(observer);
        uploadsHashMap.put(upload.getNotificationId(), upload);
        observer.setTransferListener(transferListener);

        /*For sending event to fabrics in case of failed uploads*/
        FailedUploadEvent failedUploadEvent = new FailedUploadEvent();
        failedUploadEvent.setuId(Utils.getUtils().getUserUniqueId(this));
        failedUploadEvent.setModel(Utils.getUtils().getDeviceName());
        failedUploadEvent.setNetworkType(S3EventUtil.getS3EventUtil().getNetworkClass(this));
        failedUploadEvent.setName(new File(upload.getFilePath()).getName());
        failedUploadEvent.setCarrier(S3EventUtil.getS3EventUtil().getPrimarySimOperatorName(this));
        failedUploadEvent.setUploadTime(S3EventUtil.getS3EventUtil().getCurrentTime());
        long bytesTotal = new File(upload.getFilePath()).length();
        failedUploadEvent.setSize(Formatter.formatFileSize(S3UploadService.this,bytesTotal));
        s3EventsHashMap.put(upload.getFilePath(), failedUploadEvent);
    }

    private void sendVideoUrlToCloud(final Upload upload){

        final Utils utils = Utils.getUtils();

        final String videoUrl = utils.getVideoUrl(upload);

        if (utils.videoSentToCloud(videoUrl,this))
            return;

        if (!utils.isNetworkAvailable(this)) {
            Toast.makeText(this, "Videos Uploaded : Error, check network connection", Toast.LENGTH_SHORT).show();
            return;
        }

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put(Constants.HEADER_API_KEY, utils.getUserApiKey(this));
        headerMap.put(Constants.HEADER_UID, utils.getUserUniqueId(this));
        headerMap.put(Constants.HEADER_URL, videoUrl);
        headerMap.put(Constants.HEADER_TIME, /*utils.getCurrentTime()*/utils.getVideoFileLastModifiedTime(upload.getFilePath()));

        RetrofitServiceGenerator.ApiService apiService = RetrofitServiceGenerator.createService(RetrofitServiceGenerator.ApiService.class);
        Call<UploadResponse> call = apiService.addVideoUrl(headerMap);
//        String logExceptionMsg = "COMPLETED, Id : "+upload.getTransferObserver().getId()+", filePath : "+
//                upload.getFilePath()+" time : "+headerMap.get(Constants.HEADER_TIME);
//        Crashlytics.logException(new Exception(logExceptionMsg));

        call.enqueue(new Callback<UploadResponse>() {
            @Override
            public void onResponse(Call<UploadResponse> call, Response<UploadResponse> response) {
                if (response.isSuccessful()){
                    String status = response.body().getStatus();
                    if (status.equals("success")){

                        String url = response.body().getUrl();

                        if (utils.deleteFile(upload.getFilePath()))
                            Utils.deleteFileFromMediaStore(S3UploadService.this,upload.getFilePath());

                        utils.saveCloudVideoUrl(videoUrl,S3UploadService.this);

                        Log.d("addVideoUrl","success, url = "+url);
                    } else if (status.equals("error")){

                        String desc = response.body().getDescription();
                        Log.e("addVideoUrl","error = "+desc);
                        if (response.message().equalsIgnoreCase("Unauthorized")) {
                            Utils.getUtils().clearCredentials(S3UploadService.this);
                            transferUtility.cancelAllWithType(TransferType.ANY);
                            startActivity(new Intent(S3UploadService.this, RegisterActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                        }
                    }
                } else {
                    Log.e("addVideoUrl","error = "+response.message());
                }
            }

            @Override
            public void onFailure(Call<UploadResponse> call, Throwable t) {
                t.printStackTrace();
                Log.e("addVideoUrl","error = "+t.getMessage());
            }
        });
    }
}
