package com.bempu.carecradle.s3uploads;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;

import com.bempu.carecradle.models.FailedUploadEvent;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Amit Tumkur on 10-04-2018.
 */

public class S3EventUtil {
    private static S3EventUtil s3EventUtil;
    private static final String VIDEO_HEADER_DATE_FORMAT = "yyyy/MM/dd HH:mm:ss";
    private String lastVideoHeaderTime;

    private S3EventUtil(){}

    public static S3EventUtil getS3EventUtil(){
        if (s3EventUtil == null)
            s3EventUtil = new S3EventUtil();
        return s3EventUtil;
    }

    private NetworkInfo getNetworkInfo(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    private boolean isConnectedWifi(Context context){
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_WIFI);
    }

    private boolean isConnectedMobile(Context context){
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_MOBILE);
    }

    public String getNetworkClass(Context context) {
        if (isConnectedWifi(context))
            return "WiFi";

        if (isConnectedMobile(context)) {
            TelephonyManager mTelephonyManager = (TelephonyManager)
                    context.getSystemService(Context.TELEPHONY_SERVICE);
            int networkType = mTelephonyManager.getNetworkType();
            switch (networkType) {
                case TelephonyManager.NETWORK_TYPE_GPRS:
                case TelephonyManager.NETWORK_TYPE_EDGE:
                case TelephonyManager.NETWORK_TYPE_CDMA:
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                case TelephonyManager.NETWORK_TYPE_IDEN:
                    return "2G";
                case TelephonyManager.NETWORK_TYPE_UMTS:
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                case TelephonyManager.NETWORK_TYPE_HSPA:
                case TelephonyManager.NETWORK_TYPE_EVDO_B:
                case TelephonyManager.NETWORK_TYPE_EHRPD:
                case TelephonyManager.NETWORK_TYPE_HSPAP:
                    return "3G";
                case TelephonyManager.NETWORK_TYPE_LTE:
                    return "4G";
                default:
                    return "NA";
            }
        }

        return "NA";
    }

    public String getCurrentTime() {
        Date date = new Date();
        /*11 APR 2018, 01*/
        return new SimpleDateFormat("dd MMM yyyy, h:mm a", Locale.getDefault()).format(date);
    }

    public String parseTimeFormat(String initialFormat, String finalFormat, String dateString) {
        try {
            SimpleDateFormat initialDateFormat = new SimpleDateFormat(initialFormat);
            Date date = initialDateFormat.parse(dateString);
            SimpleDateFormat updatedDateFormat = new SimpleDateFormat(finalFormat);
            return updatedDateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getPrimarySimOperatorName(Context context){
        TelephonyManager telephonyManager = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE));
        if (telephonyManager == null)
            return "";
        return telephonyManager.getSimOperatorName();
    }

    public String getFabricAttributeValue(FailedUploadEvent failedUploadEvent){
        String value = "uId:"+ failedUploadEvent.getuId()+"-"+
                "name:"+ failedUploadEvent.getName()+"-"+
                "size:"+ failedUploadEvent.getSize()+"-"+
                "model:"+ failedUploadEvent.getModel()+"-"+
                "upload time:"+ failedUploadEvent.getUploadTime()+"-"+
                "network type:"+ failedUploadEvent.getNetworkType()+"-"+
                "carrier:"+ failedUploadEvent.getCarrier()+"-"+
                "failure:"+ failedUploadEvent.getFailure()+"-"+
                "failure time:"+ failedUploadEvent.getFailureTime()+"-"+
                "percent:"+ failedUploadEvent.getUploadPercent()+"-"+
                "bytesFailed:"+ failedUploadEvent.getBytesFailed();

        return value;
    }
}
