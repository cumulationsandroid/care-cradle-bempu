/*
 * Copyright 2015 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package com.bempu.carecradle.s3uploads;

import com.amazonaws.regions.Regions;

public class S3Constants {

    /*
     * You should replace these values with your own. See the README for details
     * on what to fill in.
     */
    public static final String COGNITO_POOL_ID /*= "us-east-1:70bb46ee-7ef5-4a20-8124-97e1f9484ba5";*/
            ="us-east-1:bf9a5b7d-760e-4af8-9e91-b1239f95d751";

    /*
     * Region of your Cognito identity pool ID.
     */
    public static final String COGNITO_POOL_REGION /*= "us-east-1";*/
            = Regions.US_EAST_1.getName();

    /*
     * Note, you must first create a bucket using the S3 console before running
     * the sample (https://console.aws.amazon.com/s3/). After creating a bucket,
     * put it's name in the field below.
     */
    public static final String BUCKET_NAME = /*"bempudev"*/ "bempuvideo";

    /*
     * Region of your bucket.
     */
    public static final String BUCKET_REGION = Regions.US_EAST_1.getName();
}
