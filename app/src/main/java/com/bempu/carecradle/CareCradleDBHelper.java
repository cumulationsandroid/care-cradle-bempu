package com.bempu.carecradle;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.bempu.carecradle.models.HardwareData;
import com.bempu.carecradle.models.Upload;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by AmitT on 07-06-2017.
 */

public class CareCradleDBHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "CareCradleDb.db";
    private static final String TABLE_VIDEOS = "videos";

    private static final String VIDEO_ID = "videoId";
    private static final String VIDEO_FILE_PATH = "videoFilePath";
    private static final String VIDEO_STATUS = "videoStatus";

    private static final String TIME_STAMP = "time_stamp";
    private static final String HEART_RATE = "heat_rate";
    private static final String BATTERY_PERCENTAGE = "battery_percentage";
    private static final String SPO2 = "spo2";
    private static final String STATUS = "status";

    private static final String TABLE_SESSION = "Session";

    private SQLiteDatabase sqLiteDatabase;
    private static CareCradleDBHelper careCradleDBHelper;

    private CareCradleDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        sqLiteDatabase = this.getWritableDatabase();
    }

    public static CareCradleDBHelper getCareCradleDBHelper(Context context) {
        if (careCradleDBHelper == null)
            careCradleDBHelper = new CareCradleDBHelper(context);

        return careCradleDBHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        String CREATE_VIDEOS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_VIDEOS
                + "("
                + VIDEO_FILE_PATH + " TEXT PRIMARY KEY,"
                + VIDEO_ID + " INTEGER," /*comma is important*/
                + VIDEO_STATUS + " TEXT"
                + ")";
        createSessionValueTable(sqLiteDatabase);
        sqLiteDatabase.execSQL(CREATE_VIDEOS_TABLE);
    }

    private void createSessionValueTable(SQLiteDatabase sqLiteDatabase) {
        String CREATE_SESSION_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_SESSION
                + "("
                + VIDEO_FILE_PATH + " TEXT,"
                + HEART_RATE + " TEXT,"
                + STATUS + " TEXT,"
                + BATTERY_PERCENTAGE + " TEXT,"
                + SPO2 + " TEXT,"
                + TIME_STAMP + " TEXT"
                + ")";
        sqLiteDatabase.execSQL(CREATE_SESSION_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // Drop older table if existed
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_VIDEOS);
        // Create tables again
        onCreate(sqLiteDatabase);
    }

    public int getDbTableCount() {
        Cursor c = sqLiteDatabase.rawQuery("SELECT * FROM " + TABLE_VIDEOS, null);
        return c.getCount();
    }

    public void addVideoToDb(Upload upload, String status) {
        ContentValues values = new ContentValues();
        values.put(VIDEO_FILE_PATH, upload.getFilePath());
        values.put(VIDEO_ID, upload.getNotificationId());
        values.put(VIDEO_STATUS, status);

        // Inserting or Updating Row
        int id = (int) sqLiteDatabase.insertWithOnConflict(TABLE_VIDEOS, null, values, SQLiteDatabase.CONFLICT_IGNORE);
        if (id == -1) {
            Log.d("addVideoToDb", "video already exist " + upload.getFilePath() + " updating to " + status);
            sqLiteDatabase.update(TABLE_VIDEOS, values, VIDEO_FILE_PATH + "=?", new String[]{upload.getFilePath()});
        }
    }

    public void addSessionData(HardwareData hardwareData) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(VIDEO_FILE_PATH, hardwareData.getVideoFilePath());
        contentValues.put(TIME_STAMP, hardwareData.getTime());
        contentValues.put(HEART_RATE, hardwareData.getHr());
        contentValues.put(SPO2, hardwareData.getSpo2());
        contentValues.put(STATUS, hardwareData.getStatus());
        contentValues.put(BATTERY_PERCENTAGE, hardwareData.getBattery());

        //Inserting data
        sqLiteDatabase.insert(TABLE_SESSION, null, contentValues);
    }

    public void updateVideoStatusToDb(Upload upload, String status) {
        ContentValues values = new ContentValues();
        values.put(VIDEO_STATUS, status);
        int result = sqLiteDatabase.update(TABLE_VIDEOS, values, VIDEO_FILE_PATH + "=?", new String[]{upload.getFilePath()});
        Log.d("updateVideoStatus", "" + result);
    }

    public List<Upload> getFailedVideos() {
        List<Upload> list = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_VIDEOS;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null) {
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    if (cursor.getString(cursor.getColumnIndex(VIDEO_STATUS)) != null
                            && !cursor.getString(cursor.getColumnIndex(VIDEO_STATUS)).isEmpty()
                            && cursor.getString(cursor.getColumnIndex(VIDEO_STATUS)).equalsIgnoreCase(Upload.FAILED)) {
                        Upload upload = new Upload();
                        upload.setNotificationId(cursor.getInt(cursor.getColumnIndex(VIDEO_ID)));
                        upload.setFilePath(cursor.getString(cursor.getColumnIndex(VIDEO_FILE_PATH)));
                        list.add(upload);
                    }
                } while (cursor.moveToNext());
            }

            if (!cursor.isClosed())
                cursor.close();
        }

        return list;
    }

    public List<HardwareData> getDeviceDataListForVideo(String videoFilePath) {
        List<HardwareData> hardwareDataList = new ArrayList<>();

        String query = "SELECT * FROM " + TABLE_SESSION
                + " WHERE " + VIDEO_FILE_PATH + " = '" + videoFilePath + "'";

        Cursor cursor = getReadableDatabase().rawQuery(query, null);

        while (cursor.moveToNext()) {
            String time = cursor.getString(cursor.getColumnIndex(TIME_STAMP));
            String heart_rate = cursor.getString(cursor.getColumnIndex(HEART_RATE));
            String spo2 = cursor.getString(cursor.getColumnIndex(SPO2));
            String status = cursor.getString(cursor.getColumnIndex(STATUS));
            String batteryRemaining = cursor.getString(cursor.getColumnIndex(BATTERY_PERCENTAGE));

            HardwareData hardwareData = new HardwareData();
            hardwareData.setStatus(status);
            hardwareData.setSpo2(spo2);
            hardwareData.setBattery(Integer.valueOf(batteryRemaining));
            hardwareData.setHr(heart_rate);
            hardwareData.setTime(Utils.getUtils().getFormattedDateTime(Long.parseLong(time)));

            hardwareDataList.add(hardwareData);
        }
        return hardwareDataList;
    }

    public int clearSessionTable() {
        int deletedRowsCount;

        try {
            sqLiteDatabase.beginTransaction();
            /*deleting all rows, pass 1 to get deleted rows count*/
            deletedRowsCount = sqLiteDatabase.delete(TABLE_SESSION, "1", null);

            sqLiteDatabase.setTransactionSuccessful();
        } finally {
            sqLiteDatabase.endTransaction();
        }

        return deletedRowsCount;
    }

    public void closeParveenDb() {
        if (sqLiteDatabase != null && sqLiteDatabase.isOpen())
            sqLiteDatabase.close();
    }
}
