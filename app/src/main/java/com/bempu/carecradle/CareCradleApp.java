package com.bempu.carecradle;

import android.app.Application;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Amit Tumkur on 23-01-2018.
 */

public class CareCradleApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        /*To be initialised after 3rd party SDKs that catches uncaught exceptions*/
        Fabric.with(this, new Crashlytics());
    }
}
